-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2017 at 03:40 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_bnp`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_darurat`
--

CREATE TABLE IF NOT EXISTS `data_darurat` (
  `id_pemohon` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `hubungan` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `kelurahan` varchar(50) NOT NULL,
  `kecamatan` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `telp_rumah` int(11) NOT NULL,
  `telp_selular` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_darurat`
--

INSERT INTO `data_darurat` (`id_pemohon`, `nama_lengkap`, `hubungan`, `alamat`, `kelurahan`, `kecamatan`, `kota`, `kode_pos`, `telp_rumah`, `telp_selular`) VALUES
(1, '1', '1', '1', '1', '1', '1', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_disetujui`
--

CREATE TABLE IF NOT EXISTS `data_disetujui` (
  `id_pemohon` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `no_rek` int(11) NOT NULL,
  `cabang` varchar(50) NOT NULL,
  `kota` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_disetujui`
--

INSERT INTO `data_disetujui` (`id_pemohon`, `nama`, `nama_bank`, `no_rek`, `cabang`, `kota`) VALUES
(1, '1', '1', 1, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `data_kartu_kredit`
--

CREATE TABLE IF NOT EXISTS `data_kartu_kredit` (
  `id_pemohon` int(11) NOT NULL,
  `nama_bank` varchar(100) NOT NULL,
  `no_kartu_kredit` int(11) NOT NULL,
  `jml_limit` varchar(10) NOT NULL,
  `tgl_keanggotaan` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_kartu_kredit`
--

INSERT INTO `data_kartu_kredit` (`id_pemohon`, `nama_bank`, `no_kartu_kredit`, `jml_limit`, `tgl_keanggotaan`) VALUES
(1, '1', 1, '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `data_pekerjaan_pemohon`
--

CREATE TABLE IF NOT EXISTS `data_pekerjaan_pemohon` (
  `id_pemohon` int(11) NOT NULL,
  `jenis_pekerjaan` varchar(100) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `bidang_usaha` varchar(100) NOT NULL,
  `bagian` varchar(100) NOT NULL,
  `posisi` varchar(50) NOT NULL,
  `uraian_pekerjaan` varchar(100) NOT NULL,
  `status_kepemilikan` varchar(100) NOT NULL,
  `jml_karyawan` int(11) NOT NULL,
  `status_karyawan` varchar(100) NOT NULL,
  `masa_kontrak` varchar(100) NOT NULL,
  `lama_bekerja` varchar(10) NOT NULL,
  `alamat` text NOT NULL,
  `kelurahan` varchar(30) NOT NULL,
  `kecamatan` varchar(30) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `telp` int(11) NOT NULL,
  `no_fax` int(11) NOT NULL,
  `tgl_gaji` varchar(10) NOT NULL,
  `metode_pembayaran_gaji` varchar(30) NOT NULL,
  `pendapatan_bersih` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pekerjaan_pemohon`
--

INSERT INTO `data_pekerjaan_pemohon` (`id_pemohon`, `jenis_pekerjaan`, `nama_perusahaan`, `bidang_usaha`, `bagian`, `posisi`, `uraian_pekerjaan`, `status_kepemilikan`, `jml_karyawan`, `status_karyawan`, `masa_kontrak`, `lama_bekerja`, `alamat`, `kelurahan`, `kecamatan`, `kota`, `kode_pos`, `telp`, `no_fax`, `tgl_gaji`, `metode_pembayaran_gaji`, `pendapatan_bersih`) VALUES
(1, 'Pegawai Negeri/BUMN', 'a', 'a', 'a', 'Top Managemen', 'a', 'Sendiri', 1, 'Tetap', '1', '1', '1', '1', '1', '1', 1, 1, 1, '1', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_permohonan`
--

CREATE TABLE IF NOT EXISTS `data_permohonan` (
  `id_permohonan` int(11) NOT NULL,
  `id_pemohon` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal` varchar(10) NOT NULL,
  `jml_pokok_pinjaman` int(11) NOT NULL,
  `jangka_waktu` int(11) NOT NULL,
  `tujuan_pinjaman` varchar(100) NOT NULL,
  `kode_ao` varchar(100) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `no_rek` int(11) NOT NULL,
  `kode_cab` varchar(10) NOT NULL,
  `status` varchar(40) NOT NULL,
  `ket` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_permohonan`
--

INSERT INTO `data_permohonan` (`id_permohonan`, `id_pemohon`, `id_user`, `tanggal`, `jml_pokok_pinjaman`, `jangka_waktu`, `tujuan_pinjaman`, `kode_ao`, `nik`, `no_rek`, `kode_cab`, `status`, `ket`) VALUES
(1, 1, 1, '2017-11-03', 1, 0, 'Pendidikan', '1', '1', 1, '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `data_pribadi_pemohon`
--

CREATE TABLE IF NOT EXISTS `data_pribadi_pemohon` (
  `id_pemohon` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `nama_panggilan` varchar(50) NOT NULL,
  `no_npwp` int(11) NOT NULL,
  `no_ktp` int(11) NOT NULL,
  `masa_berlaku_ktp` varchar(30) NOT NULL,
  `kewarganegaraan` varchar(50) NOT NULL,
  `jk` varchar(20) NOT NULL,
  `tempat_lahir` varchar(80) NOT NULL,
  `tanggal_lahir` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat_ktp` text NOT NULL,
  `kelurahan_ktp` varchar(100) NOT NULL,
  `kecamatan_ktp` varchar(100) NOT NULL,
  `kota_ktp` varchar(50) NOT NULL,
  `kodepos_ktp` int(11) NOT NULL,
  `alamat_now` text NOT NULL,
  `kelurahan_now` varchar(100) NOT NULL,
  `kecamatan_now` varchar(100) NOT NULL,
  `kota_now` varchar(50) NOT NULL,
  `kode_pos_now` int(11) NOT NULL,
  `telp_rumah` int(11) NOT NULL,
  `telp_selular` int(11) NOT NULL,
  `jam_dihubungi` varchar(100) NOT NULL,
  `status_tempat_tinggal` varchar(50) NOT NULL,
  `lama_menempati` int(11) NOT NULL,
  `pendidikan` varchar(100) NOT NULL,
  `status_pernikahan` varchar(50) NOT NULL,
  `nama_pasangan` varchar(100) NOT NULL,
  `jml_anak` int(11) NOT NULL,
  `jml_tanggungan` int(11) NOT NULL,
  `jml_keluarga` int(11) NOT NULL,
  `nama_ibu_kandung` varchar(100) NOT NULL,
  `survey` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pribadi_pemohon`
--

INSERT INTO `data_pribadi_pemohon` (`id_pemohon`, `nama_lengkap`, `nama_panggilan`, `no_npwp`, `no_ktp`, `masa_berlaku_ktp`, `kewarganegaraan`, `jk`, `tempat_lahir`, `tanggal_lahir`, `email`, `alamat_ktp`, `kelurahan_ktp`, `kecamatan_ktp`, `kota_ktp`, `kodepos_ktp`, `alamat_now`, `kelurahan_now`, `kecamatan_now`, `kota_now`, `kode_pos_now`, `telp_rumah`, `telp_selular`, `jam_dihubungi`, `status_tempat_tinggal`, `lama_menempati`, `pendidikan`, `status_pernikahan`, `nama_pasangan`, `jml_anak`, `jml_tanggungan`, `jml_keluarga`, `nama_ibu_kandung`, `survey`) VALUES
(1, '1', 'a', 1, 1, '', 'a', 'Laki-Laki', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 1, 'a', 'a', 'a', 'a', 1, 1, 1, '08:00-12:00', '', 1, 'SD', 'Belum Menikah', 'a', 0, 1, 1, 'a', '1');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nik` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` int(11) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`username`, `password`, `nik`, `nama`, `alamat`, `no_hp`, `status`) VALUES
('adsa', 'asd', '12312', 'asd', 'asd', 123, 'Pencairan'),
('Novi Fibriani Prayitno', 'novifp', '6314134', 'Novi Fibriani Prayitno', 'Rancaekek', 2147483647, 'Admin Sales');

-- --------------------------------------------------------

--
-- Table structure for table `status_permohonan`
--

CREATE TABLE IF NOT EXISTS `status_permohonan` (
  `id_status` int(11) NOT NULL,
  `id_permohonan` int(11) NOT NULL,
  `status_bi` varchar(2) NOT NULL,
  `status_los` varchar(2) NOT NULL,
  `status_analis` varchar(2) NOT NULL,
  `status_survey` varchar(2) NOT NULL,
  `status_final` varchar(2) NOT NULL,
  `status_konfirmasi` varchar(2) NOT NULL,
  `status_pencairan` varchar(2) NOT NULL,
  `tgl` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_permohonan`
--

INSERT INTO `status_permohonan` (`id_status`, `id_permohonan`, `status_bi`, `status_los`, `status_analis`, `status_survey`, `status_final`, `status_konfirmasi`, `status_pencairan`, `tgl`) VALUES
(1, 1, '1', '1', '1', '1', '', '', '', '2017-11-11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_darurat`
--
ALTER TABLE `data_darurat`
  ADD KEY `id_pemohon` (`id_pemohon`);

--
-- Indexes for table `data_disetujui`
--
ALTER TABLE `data_disetujui`
  ADD KEY `id_pemohon` (`id_pemohon`), ADD KEY `id_pemohon_2` (`id_pemohon`);

--
-- Indexes for table `data_kartu_kredit`
--
ALTER TABLE `data_kartu_kredit`
  ADD KEY `id_pemohon` (`id_pemohon`), ADD KEY `id_pemohon_2` (`id_pemohon`);

--
-- Indexes for table `data_pekerjaan_pemohon`
--
ALTER TABLE `data_pekerjaan_pemohon`
  ADD KEY `id_pemohon` (`id_pemohon`);

--
-- Indexes for table `data_permohonan`
--
ALTER TABLE `data_permohonan`
  ADD PRIMARY KEY (`id_permohonan`);

--
-- Indexes for table `data_pribadi_pemohon`
--
ALTER TABLE `data_pribadi_pemohon`
  ADD PRIMARY KEY (`id_pemohon`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `status_permohonan`
--
ALTER TABLE `status_permohonan`
  ADD PRIMARY KEY (`id_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_permohonan`
--
ALTER TABLE `data_permohonan`
  MODIFY `id_permohonan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_pribadi_pemohon`
--
ALTER TABLE `data_pribadi_pemohon`
  MODIFY `id_pemohon` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `status_permohonan`
--
ALTER TABLE `status_permohonan`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
