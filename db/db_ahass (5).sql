-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2018 at 03:15 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_ahass`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_direct_sukucadang`
--

CREATE TABLE IF NOT EXISTS `detail_direct_sukucadang` (
  `id_transaksi_direct` char(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id_sukucadang` char(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `harga_sukucadang` int(11) NOT NULL,
  `qty_sukucadang` int(11) NOT NULL,
  `diskon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_direct_sukucadang`
--

INSERT INTO `detail_direct_sukucadang` (`id_transaksi_direct`, `id_sukucadang`, `harga_sukucadang`, `qty_sukucadang`, `diskon`) VALUES
('TRD-1802001', 'SC1801001', 40000, 10, 4000);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi_jasa`
--

CREATE TABLE IF NOT EXISTS `detail_transaksi_jasa` (
  `id_transaksi` char(20) COLLATE latin1_general_ci NOT NULL,
  `id_jasa` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `harga_jasa` int(11) NOT NULL,
  `qty_jasa` int(11) NOT NULL,
  `persentase` int(11) NOT NULL,
  `diskon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `detail_transaksi_jasa`
--

INSERT INTO `detail_transaksi_jasa` (`id_transaksi`, `id_jasa`, `harga_jasa`, `qty_jasa`, `persentase`, `diskon`) VALUES
('TRIM-1802001', 'JS1801001', 20000, 1, 40, 0),
('TRIM-1802001', 'JS1801002', 60000, 1, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi_sukucadang`
--

CREATE TABLE IF NOT EXISTS `detail_transaksi_sukucadang` (
  `id_transaksi` char(20) COLLATE latin1_general_ci NOT NULL,
  `id_sukucadang` char(10) COLLATE latin1_general_ci NOT NULL,
  `harga_sukucadang` int(11) NOT NULL,
  `qty_sukucadang` int(11) NOT NULL,
  `diskon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `detail_transaksi_sukucadang`
--

INSERT INTO `detail_transaksi_sukucadang` (`id_transaksi`, `id_sukucadang`, `harga_sukucadang`, `qty_sukucadang`, `diskon`) VALUES
('TRIM-1802001', 'SC1801001', 40000, 1, 0),
('TRIM-1802001', 'SC1801004', 41000000, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `head_direct`
--

CREATE TABLE IF NOT EXISTS `head_direct` (
  `id_transaksi_direct` char(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tanggal_transaksi_direct` date NOT NULL,
  `nama_konsumen` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `total_sukucadang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `head_direct`
--

INSERT INTO `head_direct` (`id_transaksi_direct`, `tanggal_transaksi_direct`, `nama_konsumen`, `total_sukucadang`) VALUES
('TRD-1802001', '2018-02-11', 'M. Faishal arsawijaya', 396000);

-- --------------------------------------------------------

--
-- Table structure for table `head_transaksi`
--

CREATE TABLE IF NOT EXISTS `head_transaksi` (
  `id_transaksi` char(20) COLLATE latin1_general_ci NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `id_konsumen` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_mekanik` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_pembayaran` int(11) NOT NULL,
  `kondisi_kendaraan` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `keluhan` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `kilometer` int(11) NOT NULL,
  `total_jasa` int(11) NOT NULL,
  `total_sukucadang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `head_transaksi`
--

INSERT INTO `head_transaksi` (`id_transaksi`, `tanggal_transaksi`, `id_konsumen`, `id_mekanik`, `id_pembayaran`, `kondisi_kendaraan`, `keluhan`, `kilometer`, `total_jasa`, `total_sukucadang`) VALUES
('TRIM-1802001', '2018-02-11', 'D1234AAA', 'EM1801001', 1, '-', '-', 20000, 80000, 41040000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee`
--

CREATE TABLE IF NOT EXISTS `tb_employee` (
  `id_employee` char(10) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `handphone` varchar(13) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `job_title` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `year_joining` year(4) NOT NULL,
  `base_sallary` int(11) NOT NULL,
  `persentase` int(11) NOT NULL,
  `stat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_employee`
--

INSERT INTO `tb_employee` (`id_employee`, `name`, `handphone`, `email`, `job_title`, `year_joining`, `base_sallary`, `persentase`, `stat`) VALUES
('EM1801001', 'Ujang', '081322097011', 'ujang@gmail.com', 'Mekanik', 2018, 2500000, 10, 1),
('EM1801002', 'Dadang', '085795237176', 'asssigit@gmail.com', 'Mekanik', 2018, 3000000, 40, 1),
('EM1801003', 'Mail', '081212111212', 'mail@dadang.com', 'Mekanik', 2018, 3000000, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_group_jasa`
--

CREATE TABLE IF NOT EXISTS `tb_group_jasa` (
  `id_group_jasa` int(11) NOT NULL,
  `kode` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `group_jasa` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `pemilik` int(11) NOT NULL,
  `mekanik` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_group_jasa`
--

INSERT INTO `tb_group_jasa` (`id_group_jasa`, `kode`, `group_jasa`, `pemilik`, `mekanik`) VALUES
(1, 'KPB', 'KPB', 0, 0),
(2, 'CS', 'COMPLATE SERVICE', 0, 0),
(3, 'HR', 'HEAVY REPAIR', 60, 40),
(4, 'LR', 'LIGHT REPAIR', 0, 0),
(6, 'OR', 'OR', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_group_sukucadang`
--

CREATE TABLE IF NOT EXISTS `tb_group_sukucadang` (
  `id_group_sukucadang` int(11) NOT NULL,
  `group_sukucadang` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_group_sukucadang`
--

INSERT INTO `tb_group_sukucadang` (`id_group_sukucadang`, `group_sukucadang`) VALUES
(1, 'a.Oli'),
(2, 'b.kampas'),
(3, 'c.Kipas');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jasa`
--

CREATE TABLE IF NOT EXISTS `tb_jasa` (
  `id_jasa` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_group_jasa` int(11) NOT NULL,
  `jasa` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `stat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_jasa`
--

INSERT INTO `tb_jasa` (`id_jasa`, `id_group_jasa`, `jasa`, `harga`, `stat`) VALUES
('JS1801001', 3, 'Pasang Kampas', 20000, 1),
('JS1801002', 1, 'Bongkar seher', 60000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_motor`
--

CREATE TABLE IF NOT EXISTS `tb_jenis_motor` (
  `id_jenis_motor` int(11) NOT NULL,
  `jenis_motor` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_jenis_motor`
--

INSERT INTO `tb_jenis_motor` (`id_jenis_motor`, `jenis_motor`) VALUES
(1, 'Matic'),
(2, 'Sport'),
(3, 'Bebek');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konsumen`
--

CREATE TABLE IF NOT EXISTS `tb_konsumen` (
  `nopol` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_motor` char(10) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `alamat` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `nohp` varchar(13) COLLATE latin1_general_ci NOT NULL,
  `ktp` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `daya_id` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `jenis_kelamin` varchar(25) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_konsumen`
--

INSERT INTO `tb_konsumen` (`nopol`, `id_motor`, `nama`, `alamat`, `nohp`, `ktp`, `daya_id`, `jenis_kelamin`) VALUES
('D1234AAA', 'MT1801001', 'Sigit', 'BCI', '081321146464', '0001292892839823', '1212', 'Laki-Laki');

-- --------------------------------------------------------

--
-- Table structure for table `tb_motor`
--

CREATE TABLE IF NOT EXISTS `tb_motor` (
  `id_motor` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_jenis_motor` int(11) NOT NULL,
  `nama_motor` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_motor`
--

INSERT INTO `tb_motor` (`id_motor`, `id_jenis_motor`, `nama_motor`) VALUES
('MT1801001', 1, 'Scoopy fi 2017'),
('MT1801002', 1, 'Beat pop 2016'),
('MT1801003', 2, 'CBR 250 Cc'),
('MT1801004', 3, 'Supra X 125');

-- --------------------------------------------------------

--
-- Table structure for table `tb_operators`
--

CREATE TABLE IF NOT EXISTS `tb_operators` (
  `id_operator` char(10) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `handphone` varchar(13) COLLATE latin1_general_ci NOT NULL,
  `address` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `sex` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(16) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `year_joining` int(11) NOT NULL,
  `stat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_operators`
--

INSERT INTO `tb_operators` (`id_operator`, `name`, `email`, `handphone`, `address`, `sex`, `username`, `password`, `year_joining`, `stat`) VALUES
('OP18010001', 'Sigit', 'asssigit@gmail.com', '085624075350', 'KOMP. BCI', 'Laki-laki', 'stevanusah', 'sigits123', 2018, 1),
('OP1801002', 'user', 'user@gmail.com', '081321146461', 'Bekasi Timur', 'Laki-Laki', 'user', 'user123', 2018, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE IF NOT EXISTS `tb_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `pembayaran` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id_pembayaran`, `pembayaran`) VALUES
(1, 'Cash'),
(2, 'Debet');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sukucadang`
--

CREATE TABLE IF NOT EXISTS `tb_sukucadang` (
  `id_sukucadang` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_group_sukucadang` int(11) NOT NULL,
  `sukucadang` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_sukucadang`
--

INSERT INTO `tb_sukucadang` (`id_sukucadang`, `id_group_sukucadang`, `sukucadang`, `harga`, `status`) VALUES
('SC1801001', 1, 'AHM Merah', 40000, 1),
('SC1801002', 1, 'AHM Biru', 45000, 1),
('SC1801003', 1, 'mpx2', 40500, 1),
('SC1801004', 1, 'mpx3', 41000000, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `head_direct`
--
ALTER TABLE `head_direct`
  ADD PRIMARY KEY (`id_transaksi_direct`);

--
-- Indexes for table `head_transaksi`
--
ALTER TABLE `head_transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `tb_employee`
--
ALTER TABLE `tb_employee`
  ADD PRIMARY KEY (`id_employee`);

--
-- Indexes for table `tb_group_jasa`
--
ALTER TABLE `tb_group_jasa`
  ADD PRIMARY KEY (`id_group_jasa`);

--
-- Indexes for table `tb_group_sukucadang`
--
ALTER TABLE `tb_group_sukucadang`
  ADD PRIMARY KEY (`id_group_sukucadang`);

--
-- Indexes for table `tb_jasa`
--
ALTER TABLE `tb_jasa`
  ADD PRIMARY KEY (`id_jasa`);

--
-- Indexes for table `tb_jenis_motor`
--
ALTER TABLE `tb_jenis_motor`
  ADD PRIMARY KEY (`id_jenis_motor`);

--
-- Indexes for table `tb_konsumen`
--
ALTER TABLE `tb_konsumen`
  ADD PRIMARY KEY (`nopol`);

--
-- Indexes for table `tb_motor`
--
ALTER TABLE `tb_motor`
  ADD PRIMARY KEY (`id_motor`);

--
-- Indexes for table `tb_operators`
--
ALTER TABLE `tb_operators`
  ADD PRIMARY KEY (`id_operator`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `tb_sukucadang`
--
ALTER TABLE `tb_sukucadang`
  ADD PRIMARY KEY (`id_sukucadang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_group_jasa`
--
ALTER TABLE `tb_group_jasa`
  MODIFY `id_group_jasa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_group_sukucadang`
--
ALTER TABLE `tb_group_sukucadang`
  MODIFY `id_group_sukucadang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_jenis_motor`
--
ALTER TABLE `tb_jenis_motor`
  MODIFY `id_jenis_motor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
