
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $this->global_m->get_get("SELECT count(id_employee) as jml FROM `tb_employee`")->jml; ?></h3>

              <p>Mekanik</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $this->global_m->get_get("SELECT count(id_sukucadang) as jml FROM `tb_sukucadang`")->jml; ?> </h3>

              <p>Jumlah sparepart</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <?php 
               $a = $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi`")->jml;
               $b = $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml2 FROM `head_direct`")->jml2;
               $c = $a+$b;

              ?>
              <h3><?= $c ?></h3>

              <p>Total Pengunjung</p>
            </div>
            <div class="icon">
              <i class="fa fa-wrench"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $this->global_m->get_get("SELECT count(id_jasa) as jml FROM `tb_jasa`")->jml; ?></h3>

              <p>Jumlah Jasa</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      

      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
             <?php
               $today =date('Y-m-d');
               $a1 = $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` where tanggal_transaksi like '%$today%'")->jml;

               $b1 = $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml2 FROM `head_direct` where tanggal_transaksi_direct like '%$today%'")->jml2;

               $c1 = $a1+$b1;
                ?>
              <h3><?=$c1; ?><sup style="font-size: 20px"></sup></h3>

              <p>Pengunjung perhari</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php
               $today =date('Y-m-d');
               echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` where tanggal_transaksi like '%$today%'")->jml; ?>
               <sup style="font-size: 20px"></sup></h3>

              <p>Work Order perhari</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>
                
                <?php
               $today =date('Y-m-d');
               echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` where tanggal_transaksi_direct like '%$today%'")->jml; ?>
              </h3>

              <p>Penjualan part perhari</p>
            </div>
            <div class="icon">
              <i class="fa fa-cog"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
        <!-- GRAFIK -->
        <div class="col-md-12 col-xs-12">
         <div class="box box-info box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Work Order Perbulan (<?php echo date('Y');?>)</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="line-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="col-md-12 col-xs-12">
         <div class="box box-info box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Grafik Penjualan Part Perbulan (<?php echo date('Y');?>)</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                
              </div>
            </div>
            <div class="box-body chart-responsive">
              <div class="chart" id="line-chart2" style="height: 300px;"></div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>

      </div> 
      <!-- /.row -->
      <!-- Main row -->

      
    </section>
