<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tabel_paket
 * @created on : Thursday, 09-Nov-2017 13:45:42
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2017
 *
 *
 */


class dashboard extends MY_Controller
{
 
    public function __construct() 
    { 
        parent::__construct();         
        //$this->load->model('tabel_pakets');
    }
    public function index(){
    	$this->template->render('view');
    }
}