<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_motor
 * @created on : Monday, 04-Dec-2017 11:04:41
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2017    
 */
 
 
class directs extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_motor
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all(){
         $query = $this->db->query("SELECT * FROM head_direct");
        return $query->result_array();
    }

    public function get_head($id){

        $this->db->select('*');
        $this->db->from('head_direct a');
        $this->db->where('id_transaksi_direct', $id);
        $result = $this->db->get();

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }

    }

    // public function get_detail_jasa($id){

    //     $this->db->select('*');
    //     $this->db->from('head_direct a');
    //     $this->db->join('detail_direct_jasa b','a.id_transaksi_direct=b.id_transaksi_direct');
    //     $this->db->join('tb_jasa c','b.id_jasa = c.id_jasa');
    //     $this->db->where('a.id_transaksi_direct', $id);
    //      $result = $this->db->get();

    //     if ($result->num_rows() > 0) 
    //     {
    //         return $result->result_array();
    //     } 
    //     else 
    //     {
    //         return array();
    //     }

    // }
    public function get_detail_sukucadang($id){

        $this->db->select('*');
        $this->db->from('head_direct a');
        $this->db->join('detail_direct_sukucadang b','a.id_transaksi_direct=b.id_transaksi_direct');
        $this->db->join('tb_sukucadang c','b.id_sukucadang=c.id_sukucadang');
        $this->db->where('a.id_transaksi_direct', $id);
         $result = $this->db->get();

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }

    }

    public function buat_kode()
    { 
        date_default_timezone_set('Asia/Jakarta');
        $y = date('y');
        $m =date('m');
        $d =date('d');
        $k = "TRD-";
        $r=$this->db->query("SELECT COUNT(id_transaksi_direct)+1 as jml FROM head_direct WHERE id_transaksi_direct != ''");
        $jml=$r->row()->jml;
        if($jml<10){
            $kode=$k.$y.$m.'00'.$jml;
        }elseif($jml<100){
            $kode==$k.$y.$m.'0'.$jml;
        }else{
            $kode==$k.$y.$m.''.$jml;
        }
        return $kode;
    }

    public function get_presentase()
    {       
        $query = $this->db->query("SELECT * from tb_presentase where id_presentase = '1'");
        return $query->row()->presentase;
        print_r($query);
        die();
        
        
    }
   
 
    public function get_jasa(){
         $query = $this->db->query("SELECT * FROM tb_jasa");
        return $query->result_array();
    }

    // public function getDataJasa($js){

    //     $query = $this->db->query("SELECT a.*,b.* FROM tb_jasa a join tb_group_jasa b on a.id_group_jasa = b.id_group_jasa where id_jasa = '$js'");
    //     return $query->row();
    //     print_r($query);
    //     die();
    // }

    public function get_sukucadang(){
         $query = $this->db->query("SELECT * FROM tb_sukucadang");
        return $query->result_array();
    }
    public function getDataSukucadang($sc){
         $query = $this->db->query("SELECT * FROM tb_sukucadang where id_sukucadang = '$sc'");
         return $query->row();
               print_r($query);
               die();
    }

     public function save() 
    {
        $data = array(

            'id_transaksi_direct' => strip_tags($this->input->post('insertkeun', TRUE)),

            'tanggal_transaksi_direct' => strip_tags($this->input->post('tanggal_service', TRUE)),

            'nama_konsumen' => strip_tags($this->input->post('nama_konsumen', TRUE))
        
        );
        
        
        $this->db->insert('head_direct', $data);
    }



    //  public function detail_save_jasa($jm) 
    // {
    //     for ($i=1;$i<=$jm;$i++){
  

    //     $data = array(

    //         'id_transaksi_direct' => strip_tags($this->input->post('id_direct', TRUE)),

    //         'id_jasa' => strip_tags($this->input->post('kode_jasa_keranjang'.$i, TRUE)),

    //         'harga_jasa' => strip_tags($this->input->post('harga_keranjang'.$i, TRUE)),

    //         'qty_jasa' => strip_tags($this->input->post('qty_keranjang'.$i, TRUE)),

        
    //     );
        
        
    //         $this->db->insert('detail_direct_jasa', $data);
    //     }


    // }

    public function detail_save_sukucadang($jm1) 
    {
        for ($j=1;$j<=$jm1;$j++){

        $data = array(

            'id_transaksi_direct' => strip_tags($this->input->post('insertkeun', TRUE)),

            'id_sukucadang' => strip_tags($this->input->post('kode_sukucadang_keranjang'.$j, TRUE)),

            'harga_sukucadang' => strip_tags($this->input->post('harga_s_keranjang'.$j, TRUE)),

            'qty_sukucadang' => strip_tags($this->input->post('qty_s_keranjang'.$j, TRUE)),

            'diskon' => strip_tags($this->input->post('diskon_value1'.$j, TRUE)),

        
        );
        
        
            $this->db->insert('detail_direct_sukucadang', $data);
        }


    }
    
  // public function upt($data){
  //          $no=1;
  //          foreach($data['qty_jasa'] as $key=>$value ):
  //            $this->db->where('id_jasa',$data['id_jasa'][$key]);
  //           $this->db->where('id_transaksi_direct',$data['id_transaksi_direct'][$key]);
  //          $this->db->update('detail_direct_jasa', array(
  //                  'qty_jasa'=>$data['qty_jasa'][$key],

  //              ));

  //               $no++;
  //          endforeach;
  //      }

       public function upt2($data){
              $no=1;
              foreach($data['qty_sukucadang'] as $key=>$value ):
                $this->db->where('id_sukucadang',$data['id_sukucadang'][$key]);
               $this->db->where('id_transaksi_direct',$data['id_transaksi_direct'][$key]);
              $this->db->update('detail_direct_sukucadang', array(
                      'qty_sukucadang'=>$data['qty_sukucadang'][$key],
                      'diskon'=>$data['dd'][$key],

                  ));

                   $no++;
              endforeach;
          }

      // public function update_edit_total_sc($id_transaksi){
      //     $data = array(

      //             'total_sukucadang' => strip_tags($this->input->post('hasil_edit_sc', TRUE)),

      //     );


      //     $this->db->where('id_transaksi_direct', $id_transaksi);
      //     $this->db->update('head_direct', $data);
      // }

             public function destroy($id)
          {       
              $this->db->where('id_transaksi_direct', $id);
              $this->db->delete(array('head_direct','detail_direct_sukucadang'));
          }


    



}
