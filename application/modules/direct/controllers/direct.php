<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tabel_paket
 * @created on : Thursday, 09-Nov-2017 13:45:42
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2017
 *
 *
 */


class direct extends MY_Controller
{
 
    public function __construct() 
    { 
        parent::__construct(); 
        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }          
        $this->load->model('directs');
    }
    public function index(){

        $data['direct'] = $this->directs->get_all();
        $this->template->render('view',$data);
        
    }

    public function save(){
        $this->directs->save();
        // $jm = $this->input->post('jums');
        $jm1 = $this->input->post('jums1');
        // $this->directs->detail_save_jasa($jm);
        $this->directs->detail_save_sukucadang($jm1);
        redirect('direct');
    }
    //     public function simpan_data(){
    //     $data=array(
    //     'qty_jasa'=>strip_tags($this->input->post('qty_jasa', TRUE)),
    //     // 'qty_sukucadang'=>strip_tags($this->input->post('qty_sukucadang', TRUE)),
    //     );
    //     $this->db->update('detail_transaksi_jasa',$data);

    //     redirect(site_url('direct'));
    // }

    public function show($id){
        $data['direct'] = $this->directs->get_head($id);
        // $data['jasa'] = $this->directs->get_detail_jasa($id);
        $data['sukucadang'] = $this->directs->get_detail_sukucadang($id);
        $this->template->render('get_show',$data);
    }

    public function edit($id){
        $data['direct'] = $this->directs->get_head($id);
        // $data['jasa'] = $this->directs->get_detail_jasa($id);
        $data['sukucadang'] = $this->directs->get_detail_sukucadang($id);
        $this->template->render('get_edit',$data);
    }

    public function add(){
        $data['action']  = 'direct/save';
        $data['kode'] = $this->directs->buat_kode();
        // $data['jasa'] = $this->directs->get_jasa();
        $data['sukucadang'] = $this->directs->get_sukucadang();
        // $data['presentase'] = $this->directs->get_presentase();
        $this->template->render('form',$data);
        
    }
  
    public function cek_kode_jasa($js)
    {
        $suir= $this->directs->getDataJasa($js)->id_jasa;
        echo $suir;
    }

    public function cek_jasa($js)
    {
        $suir= $this->directs->getDataJasa($js)->jasa;
        echo $suir;
    }
    public function cek_harga_jasa($js)
    {
        $suir= $this->directs->getDataJasa($js)->harga;
        echo $suir;
    }
    public function cek_kode_sukucadang($sc)
    {
        $suir= $this->directs->getDataSukucadang($sc)->id_sukucadang;
        echo $suir;
    }
    public function cek_nama_sukucadang($sc)
    {
        $suir= $this->directs->getDataSukucadang($sc)->sukucadang;
        echo $suir;
    }
    public function cek_harga_sukucadang($sc)
    {
        $suir= $this->directs->getDataSukucadang($sc)->harga;
        echo $suir;
    }

 public function save_edit($id_transaksi){

      // $this->directs->update_edit_total_sc($id_transaksi);

      $data['id_sukucadang']=$this->input->post('id_sukucadang');
      $data['id_transaksi_direct']=$this->input->post('id_transaksi_direct');
      $data['qty_sukucadang']=$this->input->post('qty_sukucadang');
      $data['dd']=$this->input->post('dd');
      $this->directs->upt2($data);

      // $data['id_jasa']=$this->input->post('id_jasa');
      // $data['id_transaksi_direct']=$this->input->post('id_transaksi_direct');
      // $data['qty_jasa']=$this->input->post('qty_jasa');
      // $this->directs->upt($data);
        // $jm = $this->input->post('jums');
        // $jm1 = $this->input->post('jums1');
        // $this->transaksis->detail_save_jasa($jm);
        // $this->transaksis->detail_save_sukucadang($jm1);
        redirect('direct');
    }
    public function destroy($id) 
    {        
        if ($id) 
        {
             $this->directs->destroy($id);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('direct');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('direct');
        }       
    }



}