-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 31, 2017 at 07:24 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_ahass`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_direct_jasa`
--

CREATE TABLE IF NOT EXISTS `detail_direct_jasa` (
  `id_transaksi_direct` char(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id_jasa` char(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `harga_jasa` int(11) NOT NULL,
  `qty_jasa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_direct_jasa`
--

INSERT INTO `detail_direct_jasa` (`id_transaksi_direct`, `id_jasa`, `harga_jasa`, `qty_jasa`) VALUES
('TRD-1712001', '1712001', 11, 1),
('TRD-1712002', '1712001', 11, 2),
('TRD-1712003', '1712001', 11, 1),
('TRD-1712003', '1712002', 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `detail_direct_sukucadang`
--

CREATE TABLE IF NOT EXISTS `detail_direct_sukucadang` (
  `id_transaksi_direct` char(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `id_sukucadang` char(10) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `harga_sukucadang` int(11) NOT NULL,
  `qty_sukucadang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_direct_sukucadang`
--

INSERT INTO `detail_direct_sukucadang` (`id_transaksi_direct`, `id_sukucadang`, `harga_sukucadang`, `qty_sukucadang`) VALUES
('TRD-1712001', '1712001', 45000, 2),
('TRD-1712002', '1712002', 12919, 3),
('TRD-1712003', '1712002', 12919, 1),
('TRD-1712003', '1712001', 45000, 10);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi_jasa`
--

CREATE TABLE IF NOT EXISTS `detail_transaksi_jasa` (
  `id_transaksi` char(20) COLLATE latin1_general_ci NOT NULL,
  `id_jasa` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `harga_jasa` int(11) NOT NULL,
  `qty_jasa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `detail_transaksi_jasa`
--

INSERT INTO `detail_transaksi_jasa` (`id_transaksi`, `id_jasa`, `harga_jasa`, `qty_jasa`) VALUES
('TRIM-1712001', '1712001', 11, 10),
('TRIM-1712001', 'JS1712006', 100, 100),
('TRIM-1712002', '1712001', 11, 2),
('TRIM-1712002', 'JS1712006', 100, 4),
('TRIM-1712003', '1712004', 111, 1);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi_sukucadang`
--

CREATE TABLE IF NOT EXISTS `detail_transaksi_sukucadang` (
  `id_transaksi` char(20) COLLATE latin1_general_ci NOT NULL,
  `id_sukucadang` char(10) COLLATE latin1_general_ci NOT NULL,
  `harga_sukucadang` int(11) NOT NULL,
  `qty_sukucadang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `detail_transaksi_sukucadang`
--

INSERT INTO `detail_transaksi_sukucadang` (`id_transaksi`, `id_sukucadang`, `harga_sukucadang`, `qty_sukucadang`) VALUES
('TRIM-1712001', 'SC1712003', 121212, 4),
('TRIM-1712001', '1712001', 45000, 1),
('TRIM-1712002', 'SC1712003', 121212, 10),
('TRIM-1712003', '1712002', 12919, 1);

-- --------------------------------------------------------

--
-- Table structure for table `head_direct`
--

CREATE TABLE IF NOT EXISTS `head_direct` (
  `id_transaksi_direct` char(20) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tanggal_transaksi_direct` date NOT NULL,
  `nama_konsumen` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `head_direct`
--

INSERT INTO `head_direct` (`id_transaksi_direct`, `tanggal_transaksi_direct`, `nama_konsumen`) VALUES
('TRD-1712001', '2017-12-31', 'Novi Fibriani Prayitno'),
('TRD-1712002', '2017-12-31', 'novifp'),
('TRD-1712003', '2017-12-31', 'fibriani');

-- --------------------------------------------------------

--
-- Table structure for table `head_transaksi`
--

CREATE TABLE IF NOT EXISTS `head_transaksi` (
  `id_transaksi` char(20) COLLATE latin1_general_ci NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `id_konsumen` char(7) COLLATE latin1_general_ci NOT NULL,
  `id_mekanik` char(7) COLLATE latin1_general_ci NOT NULL,
  `kondisi_kendaraan` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `keluhan` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `kilometer` int(11) NOT NULL,
  `presentase` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `head_transaksi`
--

INSERT INTO `head_transaksi` (`id_transaksi`, `tanggal_transaksi`, `id_konsumen`, `id_mekanik`, `kondisi_kendaraan`, `keluhan`, `kilometer`, `presentase`) VALUES
('TRIM-1712001', '2017-12-22', 'D1112', '1711001', 'Baik', 'Tarikan Kurang', 120000, 3),
('TRIM-1712002', '2017-12-22', 'D1112', 'Pilih M', 'baik', 'tangguh', 100, 3),
('TRIM-1712003', '2017-12-31', 'Da1120V', '1711002', 'oke', 'ddddd', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_employee`
--

CREATE TABLE IF NOT EXISTS `tb_employee` (
  `id_employee` char(10) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `handphone` varchar(13) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `job_title` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `year_joining` bigint(20) unsigned NOT NULL,
  `base_sallary` int(11) NOT NULL,
  `stat` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2019 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_employee`
--

INSERT INTO `tb_employee` (`id_employee`, `name`, `handphone`, `email`, `job_title`, `year_joining`, `base_sallary`, `stat`) VALUES
('1711001', 'Sigits', '081321146461', 'asssigit@gmail.com', 'Mekanik', 2018, 4000000, 2),
('1711002', 'j', '9898', 'iui', 'Mekanik', 989, 989, 2),
('EM1712003', 'as', '1987987798', '8798', 'Mekanik', 987, 987987, 1),
('EM1712004', 'jhjhjhj', '9809809', '098098098', 'Mekanik', 2017, 9898, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_group_jasa`
--

CREATE TABLE IF NOT EXISTS `tb_group_jasa` (
  `id_group_jasa` int(11) NOT NULL,
  `group_jasa` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_group_jasa`
--

INSERT INTO `tb_group_jasa` (`id_group_jasa`, `group_jasa`) VALUES
(1, 'LIGHT REPAIR'),
(2, 'COMPLATE SERVICE'),
(3, 'Momok Jasa'),
(4, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_group_sukucadang`
--

CREATE TABLE IF NOT EXISTS `tb_group_sukucadang` (
  `id_group_sukucadang` int(11) NOT NULL,
  `group_sukucadang` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_group_sukucadang`
--

INSERT INTO `tb_group_sukucadang` (`id_group_sukucadang`, `group_sukucadang`) VALUES
(1, 'a.Sparepartss'),
(2, 'b.Oli'),
(3, 'b.Olo');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jasa`
--

CREATE TABLE IF NOT EXISTS `tb_jasa` (
  `id_jasa` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_group_jasa` int(11) NOT NULL,
  `jasa` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `stat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_jasa`
--

INSERT INTO `tb_jasa` (`id_jasa`, `id_group_jasa`, `jasa`, `harga`, `nilai`, `stat`) VALUES
('1712001', 2, 'as', 11, 11, 0),
('1712002', 1, 'oiii', 0, 90, 0),
('1712003', 3, 'a', 9, 90, 1),
('1712004', 3, 'tff', 111, 111, 1),
('JS1712005', 0, 'aqq', 1000, 1000, 1),
('JS1712006', 1, 'uuuii', 100, 100, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_motor`
--

CREATE TABLE IF NOT EXISTS `tb_jenis_motor` (
  `id_jenis_motor` int(11) NOT NULL,
  `jenis_motor` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_jenis_motor`
--

INSERT INTO `tb_jenis_motor` (`id_jenis_motor`, `jenis_motor`) VALUES
(1, 'Matic'),
(2, 'Sport'),
(3, 'Bebek');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konsumen`
--

CREATE TABLE IF NOT EXISTS `tb_konsumen` (
  `nopol` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_motor` char(10) COLLATE latin1_general_ci NOT NULL,
  `nama` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `alamat` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `nohp` varchar(13) COLLATE latin1_general_ci NOT NULL,
  `ktp` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `daya_id` varchar(32) COLLATE latin1_general_ci NOT NULL,
  `jenis_kelamin` varchar(25) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_konsumen`
--

INSERT INTO `tb_konsumen` (`nopol`, `id_motor`, `nama`, `alamat`, `nohp`, `ktp`, `daya_id`, `jenis_kelamin`) VALUES
('D1112', '1712001', 'Dadang', ' nijiasas\r\n', '00019820192', '0910920129', '091092019', 'Laki-Laki'),
('Da1120VAP', '1712002', 'Kuda', 'jjahs\r\n', '0121211212', '121219898', '121212', 'Laki-Laki');

-- --------------------------------------------------------

--
-- Table structure for table `tb_motor`
--

CREATE TABLE IF NOT EXISTS `tb_motor` (
  `id_motor` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_jenis_motor` int(11) NOT NULL,
  `nama_motor` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_motor`
--

INSERT INTO `tb_motor` (`id_motor`, `id_jenis_motor`, `nama_motor`) VALUES
('1712001', 2, 'AAAAA'),
('1712002', 3, 'RRRR'),
('MT1712003', 2, 'dadang'),
('MT1712004', 3, 'dadang');

-- --------------------------------------------------------

--
-- Table structure for table `tb_operators`
--

CREATE TABLE IF NOT EXISTS `tb_operators` (
  `id_operator` char(10) COLLATE latin1_general_ci NOT NULL,
  `name` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `handphone` varchar(13) COLLATE latin1_general_ci NOT NULL,
  `address` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `sex` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `username` varchar(16) COLLATE latin1_general_ci NOT NULL,
  `password` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `year_joining` int(11) NOT NULL,
  `stat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_operators`
--

INSERT INTO `tb_operators` (`id_operator`, `name`, `email`, `handphone`, `address`, `sex`, `username`, `password`, `year_joining`, `stat`) VALUES
('OP1711001', 'asja', 'asas', '0809809', '8asas', 'Laki-Laki', '9ausia', 'anshasua', 2017, 2),
('OP1711002', 'M Ismail Shaleh', 'm.ismail', '08812817817', 'Buah Batu', 'perempuan', 'mail', 'mail123', 2017, 1),
('OP1711003', 'asas', 'ijiuhju', '09897', 'uash', 'Perempuan', 'aush', '21kasja', 2018, 0),
('OP1711004', 'sigit', 'asssigit@gmail.com', '081321146461', 'BCI', 'Perempuan', 'asssigit', 'sigits123', 20181, 0),
('OP1711005', 'Sigit dadang', 'asas', '9898', '98', 'Laki-Laki', '98', '98', 98, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembayaran`
--

CREATE TABLE IF NOT EXISTS `tb_pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `pembayaran` varchar(255) COLLATE latin1_general_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_pembayaran`
--

INSERT INTO `tb_pembayaran` (`id_pembayaran`, `pembayaran`) VALUES
(1, 'Cashs'),
(2, 'Debet');

-- --------------------------------------------------------

--
-- Table structure for table `tb_presentase`
--

CREATE TABLE IF NOT EXISTS `tb_presentase` (
  `id_presentase` int(11) NOT NULL,
  `presentase` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_presentase`
--

INSERT INTO `tb_presentase` (`id_presentase`, `presentase`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_sukucadang`
--

CREATE TABLE IF NOT EXISTS `tb_sukucadang` (
  `id_sukucadang` char(10) COLLATE latin1_general_ci NOT NULL,
  `id_group_sukucadang` int(11) NOT NULL,
  `sukucadang` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `tb_sukucadang`
--

INSERT INTO `tb_sukucadang` (`id_sukucadang`, `id_group_sukucadang`, `sukucadang`, `harga`, `nilai`, `status`) VALUES
('1712001', 1, 'Oli Mesin', 45000, 0, 1),
('1712002', 3, 'Olo', 12919, 192818, 2),
('SC1712003', 1, 'ASASAS', 121212, 1212, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_employee`
--
ALTER TABLE `tb_employee`
  ADD PRIMARY KEY (`id_employee`), ADD UNIQUE KEY `year_joining` (`year_joining`);

--
-- Indexes for table `tb_group_jasa`
--
ALTER TABLE `tb_group_jasa`
  ADD PRIMARY KEY (`id_group_jasa`);

--
-- Indexes for table `tb_group_sukucadang`
--
ALTER TABLE `tb_group_sukucadang`
  ADD PRIMARY KEY (`id_group_sukucadang`);

--
-- Indexes for table `tb_jasa`
--
ALTER TABLE `tb_jasa`
  ADD PRIMARY KEY (`id_jasa`);

--
-- Indexes for table `tb_jenis_motor`
--
ALTER TABLE `tb_jenis_motor`
  ADD PRIMARY KEY (`id_jenis_motor`);

--
-- Indexes for table `tb_konsumen`
--
ALTER TABLE `tb_konsumen`
  ADD PRIMARY KEY (`nopol`);

--
-- Indexes for table `tb_motor`
--
ALTER TABLE `tb_motor`
  ADD PRIMARY KEY (`id_motor`);

--
-- Indexes for table `tb_operators`
--
ALTER TABLE `tb_operators`
  ADD PRIMARY KEY (`id_operator`);

--
-- Indexes for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `tb_presentase`
--
ALTER TABLE `tb_presentase`
  ADD PRIMARY KEY (`id_presentase`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_employee`
--
ALTER TABLE `tb_employee`
  MODIFY `year_joining` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2019;
--
-- AUTO_INCREMENT for table `tb_group_jasa`
--
ALTER TABLE `tb_group_jasa`
  MODIFY `id_group_jasa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_group_sukucadang`
--
ALTER TABLE `tb_group_sukucadang`
  MODIFY `id_group_sukucadang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_jenis_motor`
--
ALTER TABLE `tb_jenis_motor`
  MODIFY `id_jenis_motor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_pembayaran`
--
ALTER TABLE `tb_pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_presentase`
--
ALTER TABLE `tb_presentase`
  MODIFY `id_presentase` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
