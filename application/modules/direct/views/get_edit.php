<script type="text/javascript">
  sub_total = 0;
  sub_total_sc = 0;  
  discons = 0;
  disconst = 0;
  dadas = 0;

  function edit_sc(no1){
    
    var edit_qty_sc = document.getElementById('harga_sc_edit'+no1).value;
    var qty_sukucadang = document.getElementById('qty_sukucadang'+no1).value;
    var dd = document.getElementById('dd'+no1).value;

    var dadas = dd / edit_qty_sc * 100;

    var edit_total_sc = edit_qty_sc * qty_sukucadang;

    var subtots = edit_total_sc - dd;

    var sub_total_sc = sub_total_sc + subtots;


    document.getElementById('total_sc'+no1).value=edit_total_sc;
    document.getElementById('sd'+no1).value=subtots;
    document.getElementById('dds'+no1).value=Math.round(dadas);

    console.log(edit_total_sc);
    console.log(dd);
    total_sc();


  }

  function total_sc()
  {
    var jml1 = eval(document.getElementById('jml1').value);

    var total1 = 0;
    for(var j=1;j<=jml1;j++){
      var sd = eval(document.getElementById('sd'+j).value);
      total1 = total1 +sd;
    }

    document.getElementById('hasil_edit_sc').value=total1;
  }

  function grandall(){

     var jml = eval(document.getElementById('jml').value);

    var total = 0;
    for(var i=1;i<=jml;i++){
      var sb = eval(document.getElementById('sb'+i).value);
      total = total +sb;
    }

    var aaa = document.getElementById('hasil_edit').value=total;

     var jml1 = eval(document.getElementById('jml1').value);

    var total1 = 0;
    for(var j=1;j<=jml1;j++){
      var sd = eval(document.getElementById('sd'+j).value);
      total1 = total1 +sd;
    }

    var bbb = document.getElementById('hasil_edit_sc').value=total1;


    var grandma = aaa + bbb;

    document.getElementById('grand').value=grandma;
  }
</script>


<div class="page-header">
    <h3>Penjualan Sparepart dengan No Transaksi - <?= $direct['id_transaksi_direct'] ?> </h3>
</div>
<br>
<form action="<?php echo base_url('direct/save_edit/'. $direct['id_transaksi_direct']);?>" method="post">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-hover">
				<thead>
					
				</thead>
				<tbody>
				
					<tr>
						<td width="200"><b>No Transaksi</td>
						<td><?= $direct['id_transaksi_direct'] ?></td>
						
					</tr>
					<tr>
						<td width="200"><b>Tanggal Transaksi</td>
						<td><?= $direct['tanggal_transaksi_direct'] ?></td>
						
					</tr>

					<tr>
						<td width="200"><b>Nama Konsumen</td>
						<td><?= $direct['nama_konsumen'] ?></td>
						
					</tr>

					
				
				</tbody>
			</table>


			
          <center><div class="breadcrumb"><b>Data Sukucadang yang digunakan</b></div></center>

		<table class="table table-hover table-condensed">

            <thead>
              <tr>
                <th style="width: auto;" class="header">No</th>
                    <th style="width: auto;">Kode Sukucadang</th>
                    <th style="width: auto;">Nama Sukucadang</th>
                    <th style="width: auto;">Harga</th>
                    <th style="width:auto;">QTY</th>
                    <th style="width: auto;">Total</th>
                    <th style="width:auto;">Disc (%)</th>
                    <th style="width:auto;">Disc (value)</th>
                    <th style="width:auto;">Sub Total</th>
                    <th hidden="" style="width:auto;">Aksi</th>

              </tr>
            </thead>
            <tbody>
            	<?php $no1 =1; ?>
            	<?php $hasills =0; ?>
            	<?php foreach ($sukucadang as $key) : ?>
              <tr>

               <td><?= $no1; ?></td>

               <td><input type="hidden" name="id_transaksi_direct[]" value="<?= $key['id_transaksi_direct'] ?>"><input type="hidden" name="id_sukucadang[]" value="<?= $key['id_sukucadang'] ?>"> <?= $key['id_sukucadang'] ?></td>

               <td><?= $key['sukucadang'] ?></td>

               <td><?= number_format($key['harga_sukucadang']) ?><input type="hidden" id="harga_sc_edit<?php echo $no1;?>"  name="harga_sc_edit" value="<?=$key['harga_sukucadang'] ?>"> </td>

               <td><input class="form-control" type="text" name="qty_sukucadang[]" id="qty_sukucadang<?php echo $no1;?>" onchange="edit_sc(<?php echo $no1;?>)"  value="<?= $key['qty_sukucadang'] ?>"></td>

              <?php $total =  $key['qty_sukucadang'] * $key['harga_sukucadang']  ?>

               <td><input readonly="" type="" value="<?= $total; ?>" name="total_sc" id="total_sc<?php echo $no1;?>" class="form-control"></td>
               <?php $hasillsl = $hasills + $total ?>

               <td><input class="form-control" type="" readonly="" name="dds" id="dds<?php echo $no1;?>"></td>

               <?php $dss = $total - $key['diskon'] ?>

               <td><input  class="form-control" type="" onchange="edit_sc(<?php echo $no1;?>)" name="dd[]" id="dd<?php echo $no1;?>" value="<?php echo $key['diskon'] ?>"></td>

               <?php $hasills = $hasills + $dss; ?>

               <td><input  class="form-control" type="" value="<?php echo $dss; ?>" readonly name="sd" id="sd<?php echo $no1;?>"></td>

               <td hidden="">
                 
                 <?php
                                  echo anchor(
                                          site_url('transaksi/delete_sukucadang/' . $key['id_transaksi'] . '/' . $key['id_sukucadang']),
                                            '<i class="fa fa-trash"></i>',
                                            'class="btn btn-sm btn-danger" data-tooltip="tooltip" data-placement="top" title="Detail"'
                                          );
                   ?>

               </td>
              </tr>

            </tbody>
        	<?php $no1++;   endforeach; ?>
        	<tr>
              	<th colspan="8">Grand Total<input type="hidden" value="<?php echo $no1-1;?>" id="jml1" name="jml1"></th>
              	<th><input class="form-control" type="" readonly value="<?= $hasills ?>" name="hasil_edit_sc" id="hasil_edit_sc"></th>
              </tr>
          </table>


         
          
          
			<?php 
	
		echo anchor(site_url('Direct'), 'Kembali', 'class="btn btn-md btn-success"');
	
	?> <input type="submit" class="btn btn-primary" name="submit" value="Simpan">
		</div>
	</div>
</div>