<div class="page-header">
    <h3>Penjualan Sparepart dengan No Transaksi - <?= $direct['id_transaksi_direct'] ?> </h3>
</div>
<br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-hover">
				<thead>
					
				</thead>
				<tbody>
				
					<tr>
						<td width="200"><b>No Transaksi</td>
						<td><?= $direct['id_transaksi_direct'] ?></td>
						
					</tr>
					<tr>
						<td width="200"><b>Tanggal Transaksi</td>
						<td><?= $direct['tanggal_transaksi_direct'] ?></td>
						
					</tr>

					<tr>
						<td width="200"><b>Nama Konsumen</td>
						<td><?= $direct['nama_konsumen'] ?></td>
						
					</tr>

					
				
				</tbody>
			</table>

          <center><div class="breadcrumb"><b>Data Part yang di beli</b></div></center>

			<table class="table table-hover table-condensed">
              
            <thead>
              <tr>
                <th class="header">No</th>
                    <th>Kode Part</th>  
                    <th>Nama Part</th> 
                    <th>Harga</th>  
                    <th>QTY</th>  
                    <th>Total</th>
              </tr>
            </thead>
            <tbody>
            	<?php $no =1; ?>
            	<?php $hasil =0; ?>
            	<?php foreach ($sukucadang as $key) : ?>
              <tr>
              
               <td><?= $no++; ?></td>
               
               <td><?= $key['id_sukucadang'] ?></td>

               <td><?= $key['sukucadang'] ?></td>

               <td><?= number_format($key['harga_sukucadang']) ?></td>

            <td><?= $key['qty_sukucadang'] ?></td>

              <?php $total =  $key['qty_sukucadang'] * $key['harga_sukucadang']  ?>
              <?php $hasil = $hasil + $total; ?>

               <td><?= number_format($total); ?></td>
              </tr>     
              
            </tbody>
        	<?php endforeach; ?>
        	<tr>
              	<th colspan="5">Sub Total</th>
              	<th><?php echo number_format($hasil)?></th>
              </tr>
          </table>
          <label><h2>Grand Total</h2></label>
          <label style="margin-left: 10px"><h2>:</h2></label>
         
          <label style="margin-left: 600px"><h2>Rp. <?php echo number_format($hasil); ?></h2></label></br>
			<?php 	
		echo anchor(site_url('Direct'), 'Kembali', 'class="btn btn-md btn-success"');
	?> 
		</div>
	</div>
</div>