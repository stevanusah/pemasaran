<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_motor
 * @created on : Monday, 04-Dec-2017 11:04:41
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2017    
 */
 
 
class work_orders extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data tb_motor
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function get_all(){
         $query = $this->db->query("SELECT * FROM head_transaksi a left join tb_konsumen b on a.id_konsumen=b.nopol left join tb_employee c on a.id_mekanik=c.id_employee join tb_pembayaran d on a.id_pembayaran=d.id_pembayaran");
        return $query->result_array();
    }

    public function buat_kode()
    { 
        date_default_timezone_set('Asia/Jakarta');
        $y = date('y');
        $m =date('m');
        $d =date('d');
        $k = "TRIM-";
        $r=$this->db->query("SELECT COUNT(id_transaksi)+1 as jml FROM head_transaksi WHERE id_transaksi != ''");
        $jml=$r->row()->jml;
        if($jml<10){
            $kode=$k.$y.$m.'00'.$jml;
        }elseif($jml<100){
            $kode==$k.$y.$m.'0'.$jml;
        }else{
            $kode==$k.$y.$m.''.$jml;
        }
        return $kode;
    }

    public function get_nopol(){
         $query = $this->db->query("SELECT a.*,b.*,c.* FROM tb_konsumen a join tb_motor b on a.id_motor = b.id_motor join tb_jenis_motor c on b.id_jenis_motor = c.id_jenis_motor");
        return $query->result_array();
    }

    public function get_mekanik()
    {       
        $this->db->SELECT('*');
        $this->db->from('tb_employee');

        $result = $this->db->get();
        return $result->result();
        
        
    }

    public function get_pembayaran(){
         $query = $this->db->query("SELECT * FROM tb_pembayaran");
        return $query->result_array();
    }

    public function save_head() 
    {
        $data = array(

            'id_transaksi' => strip_tags($this->input->post('id_transaksi', TRUE)),

            'tanggal_transaksi' => strip_tags($this->input->post('tanggal_service', TRUE)),

            'id_konsumen' => strip_tags($this->input->post('no_polisi', TRUE)),

            'id_mekanik' => strip_tags($this->input->post('nama_mekanik', TRUE)),

            'id_pembayaran' => strip_tags($this->input->post('pembayaran', TRUE)),

            'kondisi_kendaraan' => strip_tags($this->input->post('kondisi_kendaraan', TRUE)),

            'keluhan' => strip_tags($this->input->post('keluhan_konsumen', TRUE)),

            'kilometer' => strip_tags($this->input->post('kilometer', TRUE))

        
        );
        
        
        $this->db->insert('tmp_head_transaksi', $data);
    }

    public function get_jasa(){
         $query = $this->db->query("SELECT * FROM tb_jasa a join tb_group_jasa b on a.id_group_jasa = b.id_group_jasa");
        return $query->result_array();
    }

    public function get_sukucadang(){
         $query = $this->db->query("SELECT * FROM tb_sukucadang");
        return $query->result_array();
    }

    public function get_id_transaksi(){
        $this->db->SELECT('*');
        $this->db->from('head_transaksi');
        $result = $this->db->get();
    }

    // public function get_head($id){

    //     $this->db->select('*');
    //     $this->db->from('head_transaksi a');
    //     $this->db->join('tb_konsumen b','a.id_konsumen=b.nopol');
    //     $this->db->join('tb_employee c','a.id_mekanik=c.id_employee');
    //     $this->db->join('tb_pembayaran d','a.id_pembayaran=d.id_pembayaran');
    //     $this->db->where('id_transaksi', $id);
    //     $result = $this->db->get();

    //     if ($result->num_rows() == 1) 
    //     {
    //         return $result->row_array();
    //     } 
    //     else 
    //     {
    //         return array();
    //     }

    // }

    // public function get_detail_jasa($id){

    //     $this->db->select('*');
    //     $this->db->from('head_transaksi a');
    //     $this->db->join('detail_transaksi_jasa b','a.id_transaksi=b.id_transaksi');
    //     $this->db->join('tb_jasa c','b.id_jasa=c.id_jasa');
    //     $this->db->where('a.id_transaksi', $id);
    //      $result = $this->db->get();

    //     if ($result->num_rows() > 0) 
    //     {
    //         return $result->result_array();
    //     } 
    //     else 
    //     {
    //         return array();
    //     }

    // }
    // public function get_detail_sukucadang($id){

    //     $this->db->select('*');
    //     $this->db->from('head_transaksi a');
    //     $this->db->join('detail_transaksi_sukucadang b','a.id_transaksi=b.id_transaksi');
    //     $this->db->join('tb_sukucadang c','b.id_sukucadang=c.id_sukucadang');
    //     $this->db->where('a.id_transaksi', $id);
    //      $result = $this->db->get();

    //     if ($result->num_rows() > 0) 
    //     {
    //         return $result->result_array();
    //     } 
    //     else 
    //     {
    //         return array();
    //     }

    // }

    // public function buat_kode()
    // { 
    //     date_default_timezone_set('Asia/Jakarta');
    //     $y = date('y');
    //     $m =date('m');
    //     $d =date('d');
    //     $k = "TRIM-";
    //     $r=$this->db->query("SELECT COUNT(id_transaksi)+1 as jml FROM head_transaksi WHERE id_transaksi != ''");
    //     $jml=$r->row()->jml;
    //     if($jml<10){
    //         $kode=$k.$y.$m.'00'.$jml;
    //     }elseif($jml<100){
    //         $kode==$k.$y.$m.'0'.$jml;
    //     }else{
    //         $kode==$k.$y.$m.''.$jml;
    //     }
    //     return $kode;
    // }

   
    // public function get_mekanik()
    // {       
    //     $this->db->SELECT('*');
    //     $this->db->from('tb_employee');

    //     $result = $this->db->get();
    //     return $result->result();
        
        
    // }

    // public function get_nopol(){
    //      $query = $this->db->query("SELECT a.*,b.*,c.* FROM tb_konsumen a join tb_motor b on a.id_motor = b.id_motor join tb_jenis_motor c on b.id_jenis_motor = c.id_jenis_motor");
    //     return $query->result_array();
    // }
    // public function getData($sp){

    //     $query = $this->db->query("SELECT a.*,b.*,c.* FROM tb_konsumen a join tb_motor b on a.id_motor = b.id_motor join tb_jenis_motor c on b.id_jenis_motor = c.id_jenis_motor where nopol = '$sp'");
    //     return $query->row();
    //     print_r($query);
    //     die();
    // }
    // public function getDataPersenan($per){

    //     $query = $this->db->query("SELECT * FROM tb_employee where id_employee = '$per'");
    //     return $query->row();
    //     print_r($query);
    //     die();
    // }
    // public function get_jasa(){
    //      $query = $this->db->query("SELECT * FROM tb_jasa a join tb_group_jasa b on a.id_group_jasa = b.id_group_jasa");
    //     return $query->result_array();
    // }

    // public function getDataJasa($js){

    //     $query = $this->db->query("SELECT a.*,b.* FROM tb_jasa a join tb_group_jasa b on a.id_group_jasa = b.id_group_jasa where id_jasa = '$js'");
    //     return $query->row();
    //     print_r($query);
    //     die();
    // }

    // public function get_sukucadang(){
    //      $query = $this->db->query("SELECT * FROM tb_sukucadang");
    //     return $query->result_array();
    // }

    //  public function get_pembayaran(){
    //      $query = $this->db->query("SELECT * FROM tb_pembayaran");
    //     return $query->result_array();
    // }
    // public function getDataSukucadang($sc){
    //      $query = $this->db->query("SELECT * FROM tb_sukucadang where id_sukucadang = '$sc'");
    //      return $query->row();
    //            print_r($query);
    //            die();
    // }

    //  public function save() 
    // {
    //     $data = array(

    //         'id_transaksi' => strip_tags($this->input->post('id_transaksi', TRUE)),

    //         'tanggal_transaksi' => strip_tags($this->input->post('tanggal_service', TRUE)),

    //         'id_konsumen' => strip_tags($this->input->post('no_polisi', TRUE)),

    //         'id_mekanik' => strip_tags($this->input->post('nama_mekanik', TRUE)),

    //         'id_pembayaran' => strip_tags($this->input->post('pembayaran', TRUE)),

    //         'kondisi_kendaraan' => strip_tags($this->input->post('kondisi_kendaraan', TRUE)),

    //         'keluhan' => strip_tags($this->input->post('keluhan_konsumen', TRUE)),

    //         'kilometer' => strip_tags($this->input->post('kilometer', TRUE)),

    //         'total_jasa' => strip_tags($this->input->post('total1ku', TRUE)),

    //         'total_sukucadang' => strip_tags($this->input->post('total2ku', TRUE)),

        
    //     );
        
        
    //     $this->db->insert('head_transaksi', $data);
    // }

    //  public function detail_save_jasa($jm) 
    // {
    //     for ($i=1;$i<=$jm;$i++){

            


    //     $data = array(

    //         'id_transaksi' => strip_tags($this->input->post('id_transaksi', TRUE)),

    //         'id_jasa' => strip_tags($this->input->post('kode_jasa_keranjang'.$i, TRUE)),

    //         'harga_jasa' => strip_tags($this->input->post('harga_keranjang'.$i, TRUE)),

    //         'qty_jasa' => strip_tags($this->input->post('qty_keranjang'.$i, TRUE)),

    //         'persentase' => strip_tags($this->input->post('per_keranjang'.$i, TRUE)),

    //         'diskon' => strip_tags($this->input->post('diskon_value'.$i, TRUE)),

            

        
    //     );
        
        
    //         $this->db->insert('detail_transaksi_jasa', $data);
    //     }


    // }

    // public function detail_save_sukucadang($jm1) 
    // {
    //     for ($j=1;$j<=$jm1;$j++){

    //     $data = array(

    //         'id_transaksi' => strip_tags($this->input->post('id_transaksi', TRUE)),

    //         'id_sukucadang' => strip_tags($this->input->post('kode_sukucadang_keranjang'.$j, TRUE)),

    //         'harga_sukucadang' => strip_tags($this->input->post('harga_s_keranjang'.$j, TRUE)),

    //         'qty_sukucadang' => strip_tags($this->input->post('qty_s_keranjang'.$j, TRUE)),

    //         'diskon' => strip_tags($this->input->post('diskon_value1'.$j, TRUE)),

        
    //     );
        
        
    //         $this->db->insert('detail_transaksi_sukucadang', $data);
    //     }


    // }

    // public function upt($data){
    //        $no=1;
    //        foreach($data['qty_jasa'] as $key=>$value ):
    //          $this->db->where('id_jasa',$data['id_jasa'][$key]);
    //         $this->db->where('id_transaksi',$data['id_transaksi'][$key]);
    //        $this->db->update('detail_transaksi_jasa', array(
    //                'qty_jasa'=>$data['qty_jasa'][$key],
    //                'diskon'=>$data['disv'][$key],

    //            ));

    //             $no++;
    //        endforeach;
    //    }

    //     public function update_edit_total_jasa($id_transaksi){
    //     $data = array(

    //             'total_jasa' => strip_tags($this->input->post('hasil_edit', TRUE)),

    //     );


    //     $this->db->where('id_transaksi', $id_transaksi);
    //     $this->db->update('head_transaksi', $data);
    // }

    // public function update_edit_total_sc($id_transaksi){
    //     $data = array(

    //             'total_sukucadang' => strip_tags($this->input->post('hasil_edit_sc', TRUE)),

    //     );


    //     $this->db->where('id_transaksi', $id_transaksi);
    //     $this->db->update('head_transaksi', $data);
    // }

    //    public function save_edit_total_sc($data){
    //        $this->db->where('id_transaksi');
    //        $this->db->update('head_transaksi', 
    //           array(
    //                'total_sukucadang'=>$data['total_sc'],

    //            ));
    //    }

    //    public function upt2($data){
    //           $no=1;
    //           foreach($data['qty_sukucadang'] as $key=>$value ):
    //             $this->db->where('id_sukucadang',$data['id_sukucadang'][$key]);
    //            $this->db->where('id_transaksi',$data['id_transaksi'][$key]);
    //           $this->db->update('detail_transaksi_sukucadang', 
    //             array(
    //                   'qty_sukucadang'=>$data['qty_sukucadang'][$key],
    //                   'diskon'=>$data['dd'][$key],

    //               ));

    //                $no++;
    //           endforeach;
    //       }


    //    public function destroy($id)
    // {       
    //     $this->db->where('id_transaksi', $id);
    //     $this->db->delete(array('head_transaksi','detail_transaksi_sukucadang','detail_transaksi_jasa'));
    // }

    // public function delete($id,$idd)
    // {       
    //     $this->db->where('id_transaksi', $id);
    //     $this->db->where('id_jasa', $idd);
    //     $this->db->delete('detail_transaksi_jasa');
    // }
    // public function delete_sukucadang($id,$idd)
    // {       
    //     $this->db->where('id_transaksi', $id);
    //     $this->db->where('id_sukucadang', $idd);
    //     $this->db->delete('detail_transaksi_sukucadang');
    // }
    



    



}
