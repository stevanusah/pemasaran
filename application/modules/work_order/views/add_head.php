<div class="row">
  <div class="col-md-12">
	<div class="breadcrumb"><b><center>WORK OREDER</center></b></div>
  <h2><b>Langkah 1</b> - Input data pribadi</h3>
  </div>

<form  action="<?php echo $action;?>" role="form" class="form-horizontal" name="f" method="post" >

<div class="col-md-6">
<div class="panel panel-primary">
    <div class="panel-heading"><b>Data Motor</b></div>

      <div class="panel-body">


               <div class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">No Polisi<span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <select name="no_polisi" id="no_polisi" class="form-control pilih" onchange="auto()" required>
                  <!-- <option value="">~ pilih nama supir ~</option> -->
                  <option></option>
                    <?php foreach($nopol as $row2) { ?>
                        <option  <?php if ($row2['nopol']==$row2['nopol']) ?> value="<?php echo $row2['nopol'];?>"><?php echo $row2['nopol']?> | <?php echo $row2['nama']?></option>
                      <?php } ?>
                </select>
                </div>
              </div> <!--/ Name Services -->

              <div class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">Jenis Motor<span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <input type="" readonly class="form-control" name="jm" id="jm">
                </div>
              </div> <!--/ Name Services -->

              <div class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">Nama Motor<span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <input type="" readonly class="form-control" name="motor" id="motor">
                </div>
              </div> <!--/ Name Services -->

              <div style="margin-bottom: 64px" class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">Kilometer<span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <input type="" class="form-control" value="0" required onkeyup="angka(this)" name="kilometer" id="kilometer">
                </div>
              </div> <!--/ Name Services -->



      </div> <!--/ Panel Body -->

</div><!--/ Panel -->
</div>

<div class="col-md-6">
<div class="panel panel-primary">
    <div class="panel-heading"><b>Data Konsumen</b></div>

      <div class="panel-body">


               <div class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">Nama Konsumen <span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <input type="" readonly class="form-control" name="nama_konsumen" id="nama_konsumen">
                </div>
              </div> <!--/ Name Services -->

              <div class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">Alamat<span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <input type="" id="alamat_konsumen" readonly class="form-control" name="alamat_konsumen">
                </div>
              </div> <!--/ Name Services -->

              <div class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">No Hanphone<span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <input type="" readonly class="form-control" name="nohp" id="nohp">
                </div>
              </div> <!--/ Name Services -->

              <div class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">No KTP<span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <input type="" readonly class="form-control" name="ktp" id="ktp">
                </div>
              </div> <!--/ Name Services -->

              <div class="form-group">
                   <label for="name_services" class="col-sm-4 control-label">Jenis Kelamin<span class="required-input">*</span></label>
                <div class="col-sm-8">
                  <input type="" readonly class="form-control" name="jk" id="jk">
                </div>
              </div> <!--/ Name Services -->
</div><!--/ Panel -->


</div>



</div><!-- /.row -->
<input type="" name="id_transaksi" value="<?= $kode; ?>">

<div class="col-md-12">
<div class="panel panel-primary">
    <div class="panel-heading"><b><center>Data Service</center></b></div>

      <div class="panel-body">

      			<div class="form-group">
                   <label for="name_services" class="col-sm-2 control-label">Tangal Service<span class="required-input">*</span></label>
                <div class="col-sm-10">
                  <input type="" class="form-control" value="" id="tgl" name="tanggal_service">
                </div>
              </div> <!--/ Name Services -->


               <div class="form-group">
                   <label for="name_services" class="col-sm-2 control-label">Nama Mekanik <span class="required-input">*</span></label>
                <div class="col-sm-10">
                  <select class="form-control pilih" id="nama_mekanik" name="nama_mekanik" onchange="auto_per();" required>
                  	<option></option>
                      <?php foreach ($mekanik as $key): ?>
                        <option value="<?php echo $key->id_employee ?>"><?php echo $key->name ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
              </div> <!--/ Name Services -->
              

              <div class="form-group">
                   <label for="name_services" class="col-sm-2 control-label">Pembayaran<span class="required-input">*</span></label>
                <div class="col-sm-10">
                  <select class="form-control pilih" name="pembayaran" required>
                    <option></option>
                      <?php foreach ($pembayaran as $key): ?>
                        <option value="<?php echo $key['id_pembayaran'] ?>"><?php echo $key['pembayaran'] ?></option>
                      <?php endforeach; ?>
                  </select>
                </div>
              </div> <!--/ Name Services -->

              <div class="form-group">
                   <label for="name_services" class="col-sm-2 control-label">Kondisi Kendaraan<span class="required-input">*</span></label>
                <div class="col-sm-10">
                  <input type="" class="form-control" value="-" name="kondisi_kendaraan" required>
                </div>
              </div> <!--/ Name Services -->

              <div class="form-group">
                   <label for="name_services" class="col-sm-2 control-label">Keluhan Konsumen<span class="required-input">*</span></label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="keluhan_konsumen" cols="3" rows="3">-</textarea>
                </div>
              </div> <!--/ Name Services -->

      </div> <!--/ Panel Body -->

</div><!--/ Panel -->
<input type="submit" class="btn btn-success" value="Kembali" name="">
<input type="submit" onclick="return confirm('Lanjutkan Data ini?');" class="btn btn-primary btn-md" class="btn btn-danger" value="Lanjutkan" name="">
</div>

<script type="text/javascript">
  function auto(){
      var sp = document.getElementById('no_polisi').value;
      $.ajax({
        url : "<?=base_url()?>transaksi/cek_nama/"+sp,
       // type:"post",
        data:null,
        dataType: "html",
        success:function(data)
        {

          document.getElementById('nama_konsumen').value=data;
        },
        error: function(data){
        }
      })
      $.ajax({
        url : "<?=base_url()?>transaksi/cek_alamat/"+sp,
       // type:"post",
        data:null,
        dataType: "html",
        success:function(data)
        {

          document.getElementById('alamat_konsumen').value=data;
        },
        error: function(data){
        }
      })
      $.ajax({
        url : "<?=base_url()?>transaksi/cek_notelp/"+sp,
       // type:"post",
        data:null,
        dataType: "html",
        success:function(data)
        {

          document.getElementById('nohp').value=data;
        },
        error: function(data){
        }
      })
      $.ajax({
        url : "<?=base_url()?>transaksi/cek_ktp/"+sp,
       // type:"post",
        data:null,
        dataType: "html",
        success:function(data)
        {

          document.getElementById('ktp').value=data;
        },
        error: function(data){
        }
      })
      $.ajax({
        url : "<?=base_url()?>transaksi/cek_jk/"+sp,
       // type:"post",
        data:null,
        dataType: "html",
        success:function(data)
        {

          document.getElementById('jk').value=data;
        },
        error: function(data){
        }
      })
      $.ajax({
        url : "<?=base_url()?>transaksi/cek_jm/"+sp,
       // type:"post",
        data:null,
        dataType: "html",
        success:function(data)
        {

          document.getElementById('jm').value=data;
        },
        error: function(data){
        }
      })
      $.ajax({
        url : "<?=base_url()?>transaksi/cek_motor/"+sp,
       // type:"post",
        data:null,
        dataType: "html",
        success:function(data)
        {

          document.getElementById('motor').value=data;
        },
        error: function(data){
        }
      })
}
</script>

<script type="text/javascript">
  function auto_per(){
      var per = document.getElementById('nama_mekanik').value;
      $.ajax({
        url : "<?=base_url()?>transaksi/get_persenan/"+per,
       // type:"post",
        data:null,
        dataType: "html",
        success:function(data)
        {

          document.getElementById('persentase_mekanik').value=data;
        },
        error: function(data){
        }
      })
}
</script>
