<div class="row">
  <div class="col-lg-12 col-md-12">   
    <div class="breadcrumb"><b> Data Insentif</b></div>
  </div>
</div><!-- /.row -->

<section class="panel panel-default">
    <header class="panel-heading">
        <div class="row">
            <div class="col-md-8 col-xs-3">             
                
            </div>
        </div>
        <br>
    </header>
 
    <div class="panel-body">
         
          <table class="table table-hover table-condensed">
              
            <thead>
              <tr>
                <th class="header">No</th>
                
                    <th>Nama Pegawai</th>   
                
                    <th>Pekerjaan</th>     
                
                <th class="red header" align="right" width="200">Aksi</th>
              </tr>
            </thead>
            
            
            <tbody>
                <?php $number=1; ?>
               <?php foreach ($emp as $key) : ?>
              <tr>
              	<td><?php echo $number++;; ?> </td>
               
               <td><?php echo $key['name']; ?></td>
               
               <td width="200"><?php echo $key['job_title']; ?></td>
               
                <td>    
                    
                    <?php
                                  echo anchor(
                                          site_url('insentif/show/' . $key['id_employee']),
                                            '<i class="fa fa-bullseye"></i> Lakukan Perhitungan',
                                            'class="btn btn-sm btn-info" data-tooltip="tooltip" data-placement="top" title="Detail"'
                                          );
                   ?>
                                 
                </td>
              </tr>     
               <?php endforeach; ?>
            </tbody>
          </table>
         
    </div>
    
</section>
