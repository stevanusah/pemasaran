
<div class="page-header">
    <h3>Data Karyawan</h3>
</div>
<br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-hover">
				<thead>
					
				</thead>
				<tbody>
				
					<tr>
						<td width="200"><b>Tanggal</td>
						<td><input type="" id="th" class="form-control" name=""></td>
						
					</tr>
					<tr>
						<td width="200"><b>Nama Karyawan</td>
						<td><?= $dat["name"] ?></td>
						
					</tr>
					<tr>
						<td><b>Email</td>
						<td> <?= $dat["email"] ?></td>
					</tr>
					<tr>
						<td><b>Gaji Karyawan</td>
						<td>Rp <?= number_format($dat["base_sallary"]) ?></td>
					</tr>
					
				</tbody>
			</table>
			<?php 
	
		echo anchor(site_url('insentif'), 'Kembali', 'class="btn btn-sm btn-success"');
	
	?>
	<a target="_blank" href="<?php echo base_url().'insentif/cetak/'.$dat['id_employee'];?>"><span class="fa fa-print"></span> Print</a>
		</div>
	</div>
</div>