<style type="text/css">
			.table1 {
	    font-family: sans-serif;
	    font-size: 14px;
	    color: #444;
	    border-collapse: collapse;
	    width: 100%;
	    border: 1px solid #f2f5f7;
	}

	.table1 tr th{
	    background: #e40b0b;
	    color: #fff;
	    font-weight: normal;
	}

	.table1, th, td {
	    padding: 8px 14px;
	    text-align: left;
	}

	.table1 tr:hover {
	    background-color: #f5f5f5;
	}

	.table1 tr:nth-child(even) {
	    background-color: #f2f2f2;
	}
</style>

<?php $this->load->view('tablecss.php'); ?>
<?php $this->load->view('headercetak.php'); ?>


<table width="100%" >
	<tr>
		<th style="text-align: center; font-size: 20px;" ><U>PAYROLL INSENTIF</U></th>
	</tr>
</table>
<p>Payroll Insentif untuk tanggal :</p>
<table width="100%">
	<tr>
		<td width="25%">Nama Mekaneik</td>
		<td width="1%">:</td>
		<td>Sigit Stepanus Sitepu</td>
	</tr>
	<tr>
		<td>Pekerjaan</td>
		<td>:</td>
		<td>Mekanik</td>
	</tr>
	<tr>
		<td>Handphone</td>
		<td>:</td>
		<td>085292221231</td>
	</tr>
	<tr>
		<td>Email</td>
		<td>:</td>
		<td>Sigit@gmail.com</td>
	</tr>
	<tr>
		<td>Gaji</td>
		<td>:</td>
		<td>Rp. 3.000.000 ,- </td>
	</tr>
	
</table>
<br>
<table>
	<tr>
		<td></td>
	</tr>
</table><br>
Daftar hasil pekerjaan Setiap hari :
<table border="1" class="table1">
	<tr>
		<th width="15%">NO</th>
		<th width="30%">Tanggal Pekerjaan</th>
		<th width="20%">Jumlah Motor</th>
		<th width="70%">Insentif</th>
	</tr>
	<tr>
		<td>1</td>
		<td>20-11-2017</td>
		<td>2</td>
		<td>Rp. 30.000,-</td>
	</tr>
	<tr>
		<td>2</td>
		<td>21-11-2017</td>
		<td>1</td>
		<td>Rp. 60.000,-</td>
	</tr>
	<tr>
		<td>3</td>
		<td>22-11-2017</td>
		<td>5</td>
		<td>Rp. 100.000,-</td>
	</tr>
	<tr>
		<td colspan="3" align="right"><b>SUB TOTAL</b></td>

		<td><b>Rp. 190.000,-</b></td>
	</tr>
	<tr>
		<td colspan="3" align="right"><b>TOTAL</b></td>

		<td><b>Rp. 3.190.000,-</b></td>
	</tr>
</table>

</br></br>
<pre style="text-align: right;">
							<label>Mengetahui</label>
							<label>Bandung,</label>




							<label>Sigit Stepanus S</label>
</pre>


