<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of tb_jasa
 * @created on : Thursday, 09-Nov-2017 12:25:20
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2017    
 */
 
 
class insentifs extends CI_Model 
{

    public function __construct() 
    {
        parent::__construct();

    }


    /**
     *  Get All data tb_jasa
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
    public function getall() 
    {

        $this->db->select('*');
        $this->db->from('tb_employee');
        $result = $this->db->get();

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    public function get_one($id) 
    {
        $this->db->where('id_employee', $id);
        $result = $this->db->get('tb_employee');

        if ($result->num_rows() == 1) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }
    }


}
