<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tabel_paket
 * @created on : Thursday, 09-Nov-2017 13:45:42
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2017
 *
 *
 */


class insentif extends MY_Controller
{
 
    public function __construct() 
    { 
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }           
        $this->load->model('insentifs');
    }
    public function index(){

    	$data['emp'] = $this->insentifs->getall();
    	$this->template->render('insentif/view',$data);
    }
    public function show($id='') 
    {
        
            $data['dat'] = $this->insentifs->get_one($id);            
            $this->template->render('insentif/get_show',$data);
            
        
    }

    function cetak()
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';

        $pdfFilePath = "Laporan.pdf";
        
        $this->load->library('pdf');
        $mpdf = new mPDF('utf-8','A4');
        $html = '<pagefooter name="MyFooter1" content-left="{DATE j-m-Y H:i:s} | Printed By : '.$firstname.' '.$lastname.'" content-center="{PAGENO}/{nbpg}" content-right="PT Telekomunikasi Indonesia, Tbk" footer-style="font-family: serif; font-size: 8pt; font-weight: bold; font-style: italic; color: #000000;" /><setpageheader name="MyHeader1" value="on" show-this-page="1" /><setpagefooter name="MyFooter1" value="on" />';

          
        
        $html .= $this->load->view('insentif/cetak');
        //$this->pdf->pdf->WriteHTML($html);
            
            //download it.
        //$this->pdf->pdf->Output($pdfFilePath, "I"); 
    }
}