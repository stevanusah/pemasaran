<div class="row">
	<div class="col-lg-12 col-md-12">
		<div class="breadcrumb"><b>WORK ORDER</b></div>
	</div>
</div><!-- /.row -->

<section class="panel panel-default">
    <header class="panel-heading">
        <div class="row">
            <div class="col-md-8 col-xs-3">
                 <a href="<?php echo base_url('transaksi/add') ?>" class="btn btn-success btn-sm" ><span class="fa fa-plus"></span> Tambah Data</a>
            </div>
            <div class="col-md-4 col-xs-9">

            </div>
        </div>
    </header>


    <div class="panel-body">

          <table class="table table-hover table-condensed example1">

            <thead>
              <tr>
                <th class="header">No</th>

                    <th>Kode Transaksi</th>
                    <th>Nama Konsumen</th>
                    <th>Nama Mekanik</th>
                    <th>Tanggal Transaksi</th>

                <th class="red header" align="right" width="150">Aksi</th>
              </tr>
            </thead>
            <tbody>
            	<?php $no =1; ?>
            	<?php foreach ($transaksi as $key) : ?>
              <tr>

               <td><?= $no++; ?></td>

               <td><?= $key['id_transaksi'] ?></td>

               <td><?= $key['nama'] ?></td>

               <td><?= $key['name'] ?></td>

               <td><?= $key['tanggal_transaksi'] ?></td>

                <td>

                    <?php
                                  echo anchor(
                                          site_url('transaksi/show/'.$key['id_transaksi']),
                                            '<i class=""></i>Lihat Data',
                                            'class="btn btn-sm btn-info" data-tooltip="tooltip" data-placement="top" title="Detail"'
                                          );
                    

                   ?>
									 <?php if ($this->session->userdata('akses') == "1"){
																 echo anchor(
																				 site_url('transaksi/edit/'.$key['id_transaksi']),
																					 '<i class=""></i>Edit',
																					 'class="btn btn-sm btn-success" data-tooltip="tooltip" data-placement="top" title="Edit"'
																				 );
																

									?>
                   <?php
                                echo anchor(
                                          site_url('transaksi/destroy/' . $key['id_transaksi']),
                                            '<i class="glyphicon glyphicon-trash"></i>',
                                            'onclick="return confirm(\'Anda yakin..???\');" class="btn btn-sm btn-danger" data-tooltip="tooltip" data-placement="top" title="Hapus"'
                                          );
                            }

                  ?>

                </td>
              </tr>

            </tbody>
        	<?php endforeach; ?>
          </table>
    </div>

</section>
