<div class="page-header">
    <h3>WORK ORDER dengan No Transaksi - <?= $transaksi['id_transaksi'] ?> </h3>
</div>
<br>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-striped table-hover">
				<thead>
					<div class="form-group" align="right">
            <a target="_blank" href="<?php echo base_url().'laporan/cetak_transaksi/'.$transaksi['id_transaksi'];?>" class="btn btn-danger"><b>Cetak</b></a>
          </div>
				</thead>
				<tbody>
				
					<tr>
						<td width="200"><b>No Transaksi</td>
						<td><?= $transaksi['id_transaksi'] ?></td>
						
					</tr>
					<tr>
						<td width="200"><b>Tanggal Transaksi</td>
						<td><?= $transaksi['tanggal_transaksi'] ?></td>
						
					</tr>

					<tr>
						<td width="200"><b>Nama Konsumen</td>
						<td><?= $transaksi['nama'] ?></td>
						
					</tr>

					<tr>
						<td width="200"><b>Nama Mekanik</td>
						<td><?= $transaksi['name'] ?></td>
						
					</tr>

					<tr>
						<td width="200"><b>Kondisi Kendaraan</td>
						<td><?= $transaksi['kondisi_kendaraan'] ?></td>
						
					</tr>

          <tr>
            <td width="200"><b>Cara Pembayara</td>
            <td><?= $transaksi['pembayaran'] ?></td>
            
          </tr>
					
				
				</tbody>
			</table>


			<center><div class="breadcrumb"><b>Data Jasa yang digunakan</b></div></center>

			<table class="table table-hover table-condensed">
              
            <thead>
              <tr>
                <th class="header">No</th>
                    <th>Kode Jasa</th>  
                    <th>Nama Jasa</th> 
                    <th>Harga</th>  
                    <th>QTY</th>  
                    <th>Total</th>
                    <th>Diskon</th>
                    <th>Sub Total</th>
              </tr>
            </thead>
            <tbody>
            	<?php $no =1; ?>
            	<?php $hasill=0; ?>
            	<?php $totall=0; ?>
            	<?php foreach ($jasa as $key) : ?>
              <tr>
              
               <td><?= $no++; ?></td>
               
               <td><?= $key['id_jasa'] ?></td>

               <td><?= $key['jasa'] ?></td>

               <td><?= number_format($key['harga_jasa']) ?></td>

               <td><?= $key['qty_jasa'] ?></td>


               <?php $totall =  $key['qty_jasa'] * $key['harga_jasa']  ?>  
               <td><?= number_format($totall)  ?></td>
               <td><?= number_format($key['diskon'])  ?></td>

               <?php $subtot =   $totall - $key['diskon'] ?>

               <td><?= number_format($subtot)?></td>
               <?php $hasill = $hasill + $subtot; ?>
              </tr> 
                
              
            </tbody>
        	<?php endforeach; ?>
        	<tr>
              	<th colspan="7">Sub Total</th>
              	<th><?= number_format($hasill) ?></th>
              </tr>
          </table>

          <center><div class="breadcrumb"><b>Data Sukucadang yang digunakan</b></div></center>

			<table class="table table-hover table-condensed">
              
            <thead>
              <tr>
                <th class="header">No</th>
                    <th>Kode Sukucadang</th>  
                    <th>Nama Sukucadang</th> 
                    <th>Harga</th>  
                    <th>QTY</th>  
                    <th>Total</th>
                    <th>Diskon</th>
                    <th>Sub Total</th>
              </tr>
            </thead>
            <tbody>
            	<?php $no =1; ?>
            	<?php $hasil =0; ?>
            	<?php foreach ($sukucadang as $key) : ?>
              <tr>
              
               <td><?= $no++; ?></td>
               
               <td><?= $key['id_sukucadang'] ?></td>

               <td><?= $key['sukucadang'] ?></td>

               <td><?= number_format($key['harga_sukucadang']) ?></td>

               <td><?= $key['qty_sukucadang'] ?></td>

              <?php $total =  $key['qty_sukucadang'] * $key['harga_sukucadang']  ?>

               <td><?= number_format($total); ?></td>

               <td><?= number_format( $key['diskon']); ?></td>

               <?php $subtott = $total - $key['diskon'];  ?>
               <td><?= number_format($subtott); ?></td>

               <?php $hasil = $hasil + $subtott ?>
              </tr>     
              
            </tbody>
        	<?php endforeach; ?>
        	<tr>
              	<th colspan="7">Sub Total</th>
              	<th><?= number_format ($hasil); ?></th>
              </tr>
          </table>
          <label><h2>Grand Total</h2></label>
          <label style="margin-left: 10px"><h2>:</h2></label>
          <?php $gt = $hasil + $hasill ?>
          <label style="margin-left: 600px"><h2>Rp. <?php echo number_format($gt); ?></h2></label></br>
			<?php 
	
		echo anchor(site_url('Transaksi'), 'Kembali', 'class="btn btn-md btn-success"');
	
	?>
		</div>
	</div>
</div>