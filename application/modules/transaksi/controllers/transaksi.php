<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller tabel_paket
 * @created on : Thursday, 09-Nov-2017 13:45:42
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2017
 *
 *
 */


class transaksi extends MY_Controller
{
 
    public function __construct() 
    { 
        parent::__construct();    
        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }       
        $this->load->model('transaksis');
    }
    public function index(){

        $data['transaksi'] = $this->transaksis->get_all();
        $this->template->render('view',$data);
        
    }

    public function save(){
        $this->transaksis->save();
        $jm = $this->input->post('jums');
        $jm1 = $this->input->post('jums1');
        $this->transaksis->detail_save_jasa($jm);
        $this->transaksis->detail_save_sukucadang($jm1);
        redirect('transaksi');
    }

    public function show($id){
        $data['transaksi'] = $this->transaksis->get_head($id);
        $data['jasa'] = $this->transaksis->get_detail_jasa($id);
        $data['sukucadang'] = $this->transaksis->get_detail_sukucadang($id);
        $this->template->render('get_show',$data);
    }

    public function add(){
        $data['action']  = 'transaksi/save';

        $data['kode'] = $this->transaksis->buat_kode();
        $data['jasa'] = $this->transaksis->get_jasa();
        $data['sukucadang'] = $this->transaksis->get_sukucadang();
        $data['nopol'] = $this->transaksis->get_nopol();
        $data['mekanik'] = $this->transaksis->get_mekanik();
        $data['pembayaran'] = $this->transaksis->get_pembayaran();
        $this->template->render('form',$data);
        
    }

    public function get_persenan($per)
    {
        $suir= $this->transaksis->getDataPersenan($per)->persentase;
        echo $suir;
    }

    // public function memek(){
    //     $data['jasa'] = $this->transaksis->get_jasa();
    //     echo json_encode($data);
    //     // $this->load->view( 'form', $data ); 
    // }
     public function cek_nama($sp)
    {
        $suir= $this->transaksis->getData($sp)->nama;
        echo $suir;
    }
     public function cek_alamat($sp)
    {
        $suir= $this->transaksis->getData($sp)->alamat;
        echo $suir;
    }
    public function cek_notelp($sp)
    {
        $suir= $this->transaksis->getData($sp)->nohp;
        echo $suir;
    }
    public function cek_ktp($sp)
    {
        $suir= $this->transaksis->getData($sp)->ktp;
        echo $suir;
    }
     public function cek_jk($sp)
    {
        $suir= $this->transaksis->getData($sp)->jenis_kelamin;
        echo $suir;
    }
    public function cek_jm($sp)
    {
        $suir= $this->transaksis->getData($sp)->jenis_motor;
        echo $suir;
    }
    public function cek_motor($sp)
    {
        $suir= $this->transaksis->getData($sp)->nama_motor;
        echo $suir;
    }


    public function cek_kode_jasa($js)
    {
        $suir= $this->transaksis->getDataJasa($js)->id_jasa;
        echo $suir;
    }

    public function cek_jasa($js)
    {
        $suir= $this->transaksis->getDataJasa($js)->jasa;
        echo $suir;
    }

    public function persen_mekanik($js)
    {
        $suir= $this->transaksis->getDataJasa($js)->mekanik;
        echo $suir;
    }

    public function kode_persen($js)
    {
        $suir= $this->transaksis->getDataJasa($js)->kode;
        echo $suir;
    }

    public function cek_harga_jasa($js)
    {
        $suir= $this->transaksis->getDataJasa($js)->harga;
        echo $suir;
    }
     public function cek_nilai_jasa($js)
    {
        $suir= $this->transaksis->getDataJasa($js)->nilai;
        echo $suir;
    }
    public function cek_kode_sukucadang($sc)
    {
        $suir= $this->transaksis->getDataSukucadang($sc)->id_sukucadang;
        echo $suir;
    }
    public function cek_nama_sukucadang($sc)
    {
        $suir= $this->transaksis->getDataSukucadang($sc)->sukucadang;
        echo $suir;
    }
    public function cek_harga_sukucadang($sc)
    {
        $suir= $this->transaksis->getDataSukucadang($sc)->harga;
        echo $suir;
    }

    public function edit($id){
        $data['transaksi'] = $this->transaksis->get_head($id);
        $data['jasa'] = $this->transaksis->get_detail_jasa($id);
        $data['sukucadang'] = $this->transaksis->get_detail_sukucadang($id);
        $this->template->render('get_edit',$data);
    }

    public function save_edit($id_transaksi){

      // $this->transaksis->update_edit_total_jasa($id_transaksi);

      // $this->transaksis->update_edit_total_sc($id_transaksi);

      $data['id_sukucadang']=$this->input->post('id_sukucadang');
      $data['id_transaksi']=$this->input->post('id_transaksi');
      $data['qty_sukucadang']=$this->input->post('qty_sukucadang');
      $data['dd']=$this->input->post('dd');
      $this->transaksis->upt2($data);

      $data['id_jasa']=$this->input->post('id_jasa');
      $data['id_transaksi']=$this->input->post('id_transaksi');
      $data['qty_jasa']=$this->input->post('qty_jasa');
      $data['disv']=$this->input->post('disv');
      $this->transaksis->upt($data);
        // $jm = $this->input->post('jums');
        // $jm1 = $this->input->post('jums1');
        // $this->transaksis->detail_save_jasa($jm);
        // $this->transaksis->detail_save_sukucadang($jm1);
        redirect('transaksi');
    }
    public function destroy($id) 
    {        
        if ($id) 
        {
             $this->transaksis->destroy($id);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('transaksi');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('transaksi');
        }       
    }
     public function delete($id,$idd) 
    {        
        if ($id && $idd) 
        {
            $this->transaksis->delete($id,$idd);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('transaksi');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('transaksi');
        }       
    }

    public function delete_sukucadang($id,$idd) 
    {        
        if ($id && $idd) 
        {
            $this->transaksis->delete_sukucadang($id,$idd);           
             $this->session->set_flashdata('notif', notify('Data berhasil di hapus','success'));
             redirect('transaksi');
        } 
        else 
        {
            $this->session->set_flashdata('notif', notify('Data tidak ditemukan','warning'));
            redirect('transaksi');
        }       
    }

}