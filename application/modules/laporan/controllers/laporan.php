<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

/**
 * Controller katalog_pisau
 * @created on : Saturday, 26-Aug-2017 07:19:12
 * @author Daud D. Simbolon <daud.simbolon@gmail.com>
 * Copyright 2017
 *
 *
 */


class laporan extends MY_Controller
{

    public function __construct() 
    {
        parent::__construct(); 
        if($this->session->userdata('status') != "login"){
            redirect(base_url("login"));
        }          
        $this->load->model('laporans');
    }
    
	
	public function get_sukucadang()
	{
		$this->template->render('laporan/view_sukucadang');
	}

    public function get_wo_periode()
    {
        $this->template->render('laporan/view_wo_periodik');
    }

     public function get_insentif()
    {
        $data['mekanik'] = $this->laporans->get_mekanik();
        $this->template->render('laporan/view_insentif',$data);
    }

    public function get_jasa()
    {
        $this->template->render('laporan/view_jasa');
    }
	
	public function cetak_transaksi_sukucadang() 
    {
       
        $awal=$this->input->post('tgl_awal');
		$akhir=$this->input->post('tgl_akhir');
       
        
		include_once APPPATH.'/third_party/mpdf/mpdf.php';
	
			$pdfFilePath = "Laporan Transaksi Sukucadang Periode(".$awal." s.d ".$akhir.").pdf";
			
			
			//load mPDF library
			$this->load->library('pdf');
			$mpdf = new mPDF('utf-8','A4');
			$this->pdf->pdf->AddPage('P');
			
			$html = '';
	
		   
			 $data['number']         = 1;
        		$data['transaksis']       = $this->laporans->get_data_sukucadang($awal,$akhir);
				$data['awal']=$awal;
				$data['akhir']=$akhir;
				
				
			
			$html .= $this->load->view('laporan/laporan_transaksi_sukucadang',$data,true);
				//generate the PDF from the given html
			$this->pdf->pdf->WriteHTML($html);
				
				//download it.
			$this->pdf->pdf->Output($pdfFilePath, "I");
	      
    }

    public function cetak_transaksi_insentif() 
    {
        $mekanik=$this->input->post('nama_mekanik');
        $awal=$this->input->post('tgl_awal');
        $akhir=$this->input->post('tgl_akhir');
       
        
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Transaksi Sukucadang Periode(".$awal." s.d ".$akhir.").pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('L');
            
            $html = '';
    
           
                $data['number']         = 1;
                $data['insentif']       = $this->laporans->get_data_insentif($awal,$akhir,$mekanik);
                $data['mekanik']=$mekanik;
                $data['awal']=$awal;
                $data['akhir']=$akhir;
                
                
            
            $html .= $this->load->view('laporan/laporan_transaksi_insentif',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }

    public function cetak_transaksi_jasa() 
    {
       
        $awal=$this->input->post('tgl_awal');
        $akhir=$this->input->post('tgl_akhir');
       
        
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Transaksi Jasa Periode(".$awal." s.d ".$akhir.").pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('P');
            
            $html = '';
    
           
             $data['number']         = 1;
                $data['transaksis']       = $this->laporans->get_data_jasa($awal,$akhir);
                $data['awal']=$awal;
                $data['akhir']=$akhir;
                
                
            
            $html .= $this->load->view('laporan/laporan_transaksi_jasa',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }

     public function cetak_jasa() 
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Jasa.pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('P');
            
            $html = '';
    
           
            $data['number']         = 1;
            $data['transaksis']       = $this->laporans->get_jasa();
                
                
            
            $html .= $this->load->view('laporan/laporan_jasa',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }

    public function cetak_sukucadang() 
    {
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Sukucadang.pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('P');
            
            $html = '';
    
           
            $data['number']         = 1;
            $data['transaksis']       = $this->laporans->get_sukucadang();
                
                
            
            $html .= $this->load->view('laporan/laporan_sukucadang',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }

     //baru create by chandro
    public function get_selling_jasa()
    {
        $this->template->render('laporan/view_selling_jasa');
    }
    
    //baru create by chandro
    public function get_selling_sukucadang()
    {
        $this->template->render('laporan/view_selling_sukucadang');
    }

     //baru create by chandro
    public function cetak_transaksi_selling_jasa() 
    {
       
        $awal=$this->input->post('tgl_awal');
        $akhir=$this->input->post('tgl_akhir');
       
        
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Transaksi Selling Jasa Periode(".$awal." s.d ".$akhir.").pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('P');
            
            $html = '';
    
           
             $data['number']         = 1;
                $data['transaksis']       = $this->laporans->get_data_selling_jasa($awal,$akhir);
                $data['awal']=$awal;
                $data['akhir']=$akhir;
                
                
            
            $html .= $this->load->view('laporan/laporan_transaksi_selling_jasa',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }



    public function cetak_transaksi_selling_part_all() 
    {
       
       include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Penjualan Sukucadang.pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('P');
            
            $html = '';
    
           
            $data['number']         = 1;
            $data['transaksis']       = $this->laporans->get_selling_sukucadang_all();
                
                
            
            $html .= $this->load->view('laporan/laporan_selling_part_all',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }

     //

     //baru create by chandro
     public function cetak_transaksi_selling_sukucadang() 
    {
       
        $awal=$this->input->post('tgl_awal');
        $akhir=$this->input->post('tgl_akhir');
       
        
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Transaksi Selling Sukucadang Periode(".$awal." s.d ".$akhir.").pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('P');
            
            $html = '';
    
           
             $data['number']         = 1;
                $data['transaksis']       = $this->laporans->get_data_selling_sukucadang($awal,$akhir);
                $data['awal']=$awal;
                $data['akhir']=$akhir;
                
                
            
            $html .= $this->load->view('laporan/laporan_transaksi_selling_sukucadang',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }

    public function cetak_workorder_all() 
    {
        
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Transaksi WorkOrder.pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('P');
            
            $html = '';
    
           
             $data['number']         = 1;
             $data['transaksis']       = $this->laporans->get_workorder_all();
              // $data['transaksis_detail']       = $this->laporans->get_workorder_all_detail();
              // $data['transaksis_detail_sc']       = $this->laporans->get_workorder_all_detail_sc();
               
                
            
            $html .= $this->load->view('laporan/laporan_work_order',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }


    public function cetak_workorder_periodik() 
    {
        
         $awal=$this->input->post('tgl_awal');
         $akhir=$this->input->post('tgl_akhir');
       
        
        include_once APPPATH.'/third_party/mpdf/mpdf.php';
    
            $pdfFilePath = "Laporan Work Order Periode(".$awal." s.d ".$akhir.").pdf";
            
            
            //load mPDF library
            $this->load->library('pdf');
            $mpdf = new mPDF('utf-8','A4');
            $this->pdf->pdf->AddPage('P');
            
            $html = '';
    
           
             $data['number']         = 1;
                $data['transaksis']       = $this->laporans->get_wo_periode($awal,$akhir);
                $data['awal']=$awal;
                $data['akhir']=$akhir;
                
                
            
            $html .= $this->load->view('laporan/laporan_wo_periodik',$data,true);
                //generate the PDF from the given html
            $this->pdf->pdf->WriteHTML($html);
                
                //download it.
            $this->pdf->pdf->Output($pdfFilePath, "I");
          
    }

    public function cetak_transaksi($id) 
  {
    include_once APPPATH.'/third_party/mpdf/mpdf.php';
      
    $pdfFilePath = "Laporan Transaksi No.(".$id.").pdf";
          
    //load mPDF library
    $this->load->library('pdf');
    $mpdf = new mPDF('utf-8','A4');
    $this->pdf->pdf->AddPage('P');
          
      $html = '';
    
    $data['transaksi']       = $this->laporans->get_one_trans($id);
    $data['transjasa']       = $this->laporans->get_jasa_trans($id);
    $data['transsk']       = $this->laporans->get_sukucadang_trans($id);
            
    $html .= $this->load->view('laporan/laporan_transaksi',$data,true);
    //generate the PDF from the given html
    $this->pdf->pdf->WriteHTML($html);
            
    //download it.
    $this->pdf->pdf->Output($pdfFilePath, "I");
            
  }

   // New Code Amf
    public function get_excel()
    {
        $data['getlist'] = $this->laporans->getListMotor();
        $data['getlistsc'] = $this->laporans->getlistSukucadang();
        $this->load->view('laporan/laporan_lbb',$data);
    }
    
    public function get_excel1()
    {
        $data['getlistmekanik']=$this->laporans->getListMekanik();   
        $this->load->view('laporan/laporan_mekanik',$data);
    }
    
    // Create tgl : 15/03/2018
    public function get_omset_sk_periode()
    {
        $this->template->render('laporan/view_omset_sk_periodik');
    }

    public function cetak_omset_sk_periodik() 
    {
        
         $awal=$this->input->post('tgl_awal');  
         $akhir=$this->input->post('tgl_akhir');       
    
           
                $data['transaksis']       = $this->laporans->get_omset_sk_periode($awal,$akhir);
                $data['awal']=$awal;  
                $data['akhir']=$akhir;                
                
            
            $this->load->view('laporan/laporan_omset_sk',$data);        
    }


     public function get_omset_jasa_periode()
    {
        $this->template->render('laporan/view_omset_jasa_periodik');
    }

    public function cetak_omset_jasa_periodik() 
    {
        
         $awal=$this->input->post('tgl_awal');       
           $akhir=$this->input->post('tgl_akhir');
    
           

                $data['transaksis']       = $this->laporans->get_omset_jasa_periode($awal,$akhir);
                $data['awal']=$awal;
                $data['akhir']=$akhir;                
                
            
            $this->load->view('laporan/laporan_omset_jasa',$data);        
    }

     public function get_lbb_periodik()
    {
        $this->template->render('laporan/view_lbb_periodik');
    }

    public function cetak_lbb_periodik() 
    {
            $data['getlist'] = $this->laporans->getListMotor();
            $data['getlistsc'] = $this->laporans->getlistSukucadang();
        
         $awal=$this->input->post('tgl_awal');
        $akhir=$this->input->post('tgl_akhir');      
    
           

                $data['transaksis']       = $this->laporans->get_lbb_periode($awal,$akhir);
                $data['awal']=$awal;                
                $data['akhir']=$akhir;                
                
            
            $this->load->view('laporan/laporan_lbb_periodik',$data);        
    }
    
}

?>
