<div class="row">
	<div class="col-lg-12 col-md-12">		
		<?php 
                
                echo "<h3>Laporan Omset Sukucadang Periodik</h3>";		
                echo $this->session->flashdata('notify'); 
                
                ?>
	</div>
</div><!-- /.row -->
<div class="well">
</div>
<div class="row">
	<div class="col-md-12">
    <form action="<?php echo base_url('laporan/cetak_omset_sk_periodik');?>" method="post" target="_blank">
    	<table class="table">
        	<tr>
            	<td width="20%"><strong>Awal Periode</strong></td>
                <td width="2%"><strong>:</strong></td>
                <td><input required="required" id="tgl_awal" name="tgl_awal" type="text" class="form-control nonaktif" /></td>
            </tr>
            <tr>
                <td width="20%"><strong>Akhir Periode</strong></td>
                <td width="2%"><strong>:</strong></td>
                <td><input required="required" id="tgl_akhir" name="tgl_akhir" type="text" class="form-control nonaktif" /></td>
            </tr>

            <tr>
            	<td width="20%"></td>
                <td></td>
                <td><input type="submit" class="btn btn-success" value="Cetak" /></td>
            </tr>
        </table>
        </form>
    </div>
</div>
<script type="text/javascript">
$(function() {
    $( "#tgl_awal,#tgl_akhir" ).datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		endDate: 'd',
		
		 
		});
  	});
</script>