<?php if(!defined('BASEPATH'))exit('Tidak Punya akses');

header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename=Draft Lbb.xls'); //tell browser what's the file nam
header('Cache-Control: max-age=0');
?>
<h3>LAPORAN PENDAPATAN BENGKEL</h3>
<br> 
<table border="1" align="left">
  <tr>
    <th style="background-color: #ffc000" align="left" width="40%"  colspan="2">Pendapatan Jasa</th>
      <tr>
	  	<td width="20px">KARTU SERVICE BERKALA</td>
	  	<td style="background-color: #66ff99; " width="20%"><?php echo $this->global_m->get_get("SELECT sum(a.qty_jasa * a.harga_jasa) as totaljasa FROM detail_transaksi_jasa a JOIN tb_jasa b on a.id_jasa=b.id_jasa JOIN tb_group_jasa c on b.id_group_jasa=c.id_group_jasa WHERE c.kode = 'KPB'")->totaljasa; ?></td>
	  </tr>
	  <tr>
	  	<td width="20px">COMPLATE SERVICE</td>
	  	<td style="background-color: #66ff99; " width="20%"><?php echo $this->global_m->get_get("SELECT sum(a.qty_jasa * a.harga_jasa) as totaljasa FROM detail_transaksi_jasa a JOIN tb_jasa b on a.id_jasa=b.id_jasa JOIN tb_group_jasa c on b.id_group_jasa=c.id_group_jasa WHERE c.kode = 'CS'")->totaljasa; ?></td>
	  </tr>
	  <tr>
	  	<td width="20px">HEAVY REPAIR</td>
	  	<td style="background-color: #66ff99; " width="20%"><?php echo $this->global_m->get_get("SELECT sum(a.qty_jasa * a.harga_jasa) as totaljasa FROM detail_transaksi_jasa a JOIN tb_jasa b on a.id_jasa=b.id_jasa JOIN tb_group_jasa c on b.id_group_jasa=c.id_group_jasa WHERE c.kode = 'HR'")->totaljasa; ?></td>
	  </tr>
	  <tr>
	  	<td width="20px">LIGHT REPAIR</td>
	  	<td style="background-color: #66ff99; " width="20%"><?php echo $this->global_m->get_get("SELECT sum(a.qty_jasa * a.harga_jasa) as totaljasa FROM detail_transaksi_jasa a JOIN tb_jasa b on a.id_jasa=b.id_jasa JOIN tb_group_jasa c on b.id_group_jasa=c.id_group_jasa WHERE c.kode = 'LR'")->totaljasa; ?></td>
	  </tr>
	  <tr>
	  	<td width="20px">OR</td>
	  	<td style="background-color: #66ff99; " width="20%"><?php echo $this->global_m->get_get("SELECT sum(a.qty_jasa * a.harga_jasa) as totaljasa FROM detail_transaksi_jasa a JOIN tb_jasa b on a.id_jasa=b.id_jasa JOIN tb_group_jasa c on b.id_group_jasa=c.id_group_jasa WHERE c.kode = 'OR'")->totaljasa; ?></td>
	  </tr>

  </tr>
</table>
	<br></br>
<table border="1" align="left">
  <tr>
    <th style="background-color: #ffc000" align="left" width="40%"  colspan="2">Pendapatan Sparepart</th>
    <?php foreach ($getlistsc as $key) : ?>
      <tr>
	  	<td width="20px"><?= $key['group_sukucadang'] ?></td>
	  	<td style="background-color: #66ff99; " width="20%"><?php 
                  $total_1=$this->db->query("SELECT sum(a.qty_sukucadang * a.harga_sukucadang - a.diskon) as total1 from detail_transaksi_sukucadang a JOIN tb_sukucadang b on a.id_sukucadang=b.id_sukucadang JOIN tb_group_sukucadang c on b.id_group_sukucadang=c.id_group_sukucadang  where c.group_sukucadang='".$key['group_sukucadang']."' ")->row()->total1;

                   $total_2=$this->db->query("SELECT sum(a.qty_sukucadang * a.harga_sukucadang - a.diskon) as total2 from detail_direct_sukucadang a JOIN tb_sukucadang b on a.id_sukucadang=b.id_sukucadang JOIN tb_group_sukucadang c on b.id_group_sukucadang=c.id_group_sukucadang  where c.group_sukucadang='".$key['group_sukucadang']."' ")->row()->total2;

                
                echo number_format($total_1+$total_2) ;

                ?></td>
	  </tr>
	<?php endforeach; ?>
  </tr>
</table>


   
<br>

<table border="1">
	<!-- Header -->
	<tr>
		<th style="background-color: #ffc000" colspan="7" >Data Motor</th>
	</tr>
	<tr>
		<th style="background-color: #ffc000" >Motor</th>
		<th style="background-color: #ffc000" >Total Unit Entry</th>
		<th style="background-color: #ffc000">KPB</th>
		<th style="background-color: #ffc000">Complate Service</th>
		<th style="background-color: #ffc000">Heavy Repair</th>
		<th style="background-color: #ffc000">Light Repair</th>
		<th style="background-color: #ffc000">OR</th>

	</tr>

	<!-- Body -->
	<?php foreach ($getlist as $key) : ?>
	<tr>
	
		<td><?= $key['nama_motor']; ?></td>
		<td  style="background-color: #66ff99">
			<?php 

				$nm=$this->db->query("SELECT COUNT(a.id_transaksi) as nama_m FROM head_transaksi a JOIN tb_konsumen b on a.id_konsumen=b.nopol JOIN tb_motor c on b.id_motor=c.id_motor  where c.nama_motor='".$key['nama_motor']."' ")->row()->nama_m;
				echo $nm;

			?>
		</td>
		<td  style="background-color: #66ff99">
			
			<?php 

				$nm=$this->db->query("SELECT COUNT(e.kode) as kode_m FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_konsumen c ON a.id_konsumen=c.nopol JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa JOIN tb_motor f on c.id_motor=f.id_motor where e.kode='KPB' and f.nama_motor='".$key['nama_motor']."'")->row()->kode_m;
				echo $nm;

			?>


		</td>
		<td  style="background-color: #66ff99">
			<?php 

				$nm=$this->db->query("SELECT COUNT(e.kode) as kode_m FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_konsumen c ON a.id_konsumen=c.nopol JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa JOIN tb_motor f on c.id_motor=f.id_motor where e.kode='CS' and f.nama_motor='".$key['nama_motor']."'")->row()->kode_m;
				echo $nm;

			?>
		</td>
		<td  style="background-color: #66ff99">
			
			<?php 

				$nm=$this->db->query("SELECT COUNT(e.kode) as kode_m FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_konsumen c ON a.id_konsumen=c.nopol JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa JOIN tb_motor f on c.id_motor=f.id_motor where e.kode='HR' and f.nama_motor='".$key['nama_motor']."'")->row()->kode_m;
				echo $nm;

			?>

		</td>
		<td  style="background-color: #66ff99">
			
			<?php 

				$nm=$this->db->query("SELECT COUNT(e.kode) as kode_m FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_konsumen c ON a.id_konsumen=c.nopol JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa JOIN tb_motor f on c.id_motor=f.id_motor where e.kode='LR' and f.nama_motor='".$key['nama_motor']."'")->row()->kode_m;
				echo $nm;

			?>

		</td>
		<td  style="background-color: #66ff99">
			
			<?php 

				$nm=$this->db->query("SELECT COUNT(e.kode) as kode_m FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_konsumen c ON a.id_konsumen=c.nopol JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa JOIN tb_motor f on c.id_motor=f.id_motor where e.kode='OR' and f.nama_motor='".$key['nama_motor']."'")->row()->kode_m;
				echo $nm;

			?>

		</td>
	
	</tr>
	<?php endforeach; ?>
	
	
	
</table>
