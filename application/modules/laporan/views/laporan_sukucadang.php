<style>
  .tables{
	  
      font-size:13px;
	  border-collapse: collapse;
      width: 100%;
	  height:100%;
      margin: 0 auto;
  }
  .tables th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: center;
  }
  .tables td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
  }
  </style>
  <?php $this->load->view('laporan/headercetak.php'); ?>
  <h3 style="text-align:center"><center>Laporan Sparepart</center></h3>
  
  <br>
  <table class="tables">
              
            <thead>
              <tr>
                <th class="header" style="width: auto;">No</th>
                
                    <th style="width: auto;">ID Sukucadang</th>   
                
                    <th style="width: auto;">Group Sukucadang</th>   
                
                    <th style="width: auto;">Sukucadang</th>    
                
                    <th style="width: auto;">Harga</th>   

              </tr>
            </thead>
            
            
            <tbody>
                
               <?php 
               $total=0; foreach ($transaksis as $data) :
               ?>
              <tr>
              	<td><?php echo $number++;; ?> </td>
               
                <td><?php echo $data['id_sukucadang']; ?></td>
               
                <td><?php echo $data['group_sukucadang']; ?></td>

                <td><?php echo $data['sukucadang']; ?></td>

                <td><?php echo "Rp. ".number_format($data['harga'],0,'.',',')."" ?></td>

                <?php  
                  $gtotal=$gtotal+$data['harga'];
                ?>

              <?php endforeach; ?>

                <tr>
                  <td colspan="4" align="center"><b>Total</b></td>
                  <td><b><?php echo 'Rp. '.number_format($gtotal,0,'.',',');?></b></td>
               </tr>
               
               
            </tbody>
          </table>