<style>
  .tables{
	  
      font-size:13px;
	  border-collapse: collapse;
      width: 100%;
	  height:100%;
      margin: 0 auto;
  }
  .tables th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: center;
  }
  .tables td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
  }
  </style>
  <?php $this->load->view('laporan/headercetak.php'); ?>
  <h3 style="text-align:center"><center>Laporan Insentif Mekanik</center></h3>
  <h4 style="text-align:center"><center>Periode (<?php echo $awal;?> s.d. <?php echo $akhir;?>) Atas Nama <?php echo $this->global_m->get_one_field('name','tb_employee','id_employee',$mekanik); ?></center></h4>
  <br>
  <table class="tables">
              
            <thead>
              <tr>
                <th class="header" style="width: auto;">No</th>
                
                    <th style="width: auto;">No Transaksi</th>  

                    <th style="width: auto;">Tanggal Transaksi</th>   
                
                    <th style="width: auto;">Nama Konsumen</th>

                    <th style="width: auto;">Group Jasa</th>

                    <th style="width: auto;">Jasa</th>  
                
                    <th style="width: auto;">Harga Jasa</th>    

                    <th style="width: auto;">Nilai Jasa</th>  

                    <th style="width: auto;">Persentase</th>  
                    
                    <th style="width: auto;">Insentif</th> 

              </tr>
            </thead>
            
            
            <tbody>
                
               <?php 
               $qty=0;
               $totalku = 0;
               $jm=0;$tl=0; foreach ($insentif as $data) :
               $totalku = $totalku + $data['insentif'];

               ?>
              <tr>
              	<td><?php echo $number++;; ?> </td>
               
                <td><?php echo $data['id_transaksi'] ?></td>
               
                <td><?php echo $data['tanggal_transaksi'] ?></td>

                <td><?php echo $data['nama'] ?></td>

                <td><?php echo $data['group_jasa']  ?></td>

                <td><?php echo $data['jasa'] ?></td>

                <td><?php echo number_format($data['harga']) ?></td>

                <td><?php echo number_format($data['nilai']) ?></td>

                <td><?php echo $data['permek'] ?></td>

                <td><?php echo number_format($data['insentif']) ?></td>

              <?php endforeach; ?>

                <tr>
                  <td rowspan="2" colspan="9" align="right"><b>Grand Total</b></td> 
                </tr>
                <tr>
                  <td><b><center><?php echo number_format($totalku);?></center></b></td>
               </tr>
               
               
            </tbody>
          </table>