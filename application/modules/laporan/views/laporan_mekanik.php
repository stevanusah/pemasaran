<?php if(!defined('BASEPATH'))exit('Tidak Punya akses');

header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename=Laporan Mekanik.xls'); //tell browser what's the file nam
header('Cache-Control: max-age=0');
?>
<h2>LAPORAN MEKANIK</h2>
<br>


<table border="1">
	<!-- Header -->
	<tr>
		<th style="background-color: #ffc000">No</th>
		<th style="background-color: #ffc000">Mekanik</th>
		<th style="background-color: #ffc000">KPB</th>
		<th style="background-color: #ffc000">Complate Service</th>
		<th style="background-color: #ffc000">Heavy Repair</th>
		<th style="background-color: #ffc000">Light Repair</th>
		<th style="background-color: #ffc000">OR</th>

	</tr>

	<!-- Body -->
	<?php 
	$no =1 ;
	foreach ($getlistmekanik as $key) : ?>
	<tr>
		<td><?= $no++; ?></td>
		<td ><?= $key['name'] ?></td>
		<td  style="background-color: #66ff99">
			
			<?php 

				$nm=$this->db->query("SELECT COUNT(b.id_transaksi) as j_kpb FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_employee c on a.id_mekanik=c.id_employee JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa where e.kode='KPB' and c.id_employee='".$key['id_employee']."'")->row()->j_kpb;
				echo $nm;

			?>

		</td>
		<td  style="background-color: #66ff99">
			
			<?php 

				$nm=$this->db->query("SELECT COUNT(b.id_transaksi) as j_kpb FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_employee c on a.id_mekanik=c.id_employee JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa where e.kode='CS' and c.id_employee='".$key['id_employee']."'")->row()->j_kpb;
				echo $nm;

			?>

		</td>
		<td  style="background-color: #66ff99">
			
			<?php 

				$nm=$this->db->query("SELECT COUNT(b.id_transaksi) as j_kpb FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_employee c on a.id_mekanik=c.id_employee JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa where e.kode='HR' and c.id_employee='".$key['id_employee']."'")->row()->j_kpb;
				echo $nm;

			?>

		</td>
		<td  style="background-color: #66ff99">
			
			<?php 

				$nm=$this->db->query("SELECT COUNT(b.id_transaksi) as j_kpb FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_employee c on a.id_mekanik=c.id_employee JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa where e.kode='LR' and c.id_employee='".$key['id_employee']."'")->row()->j_kpb;
				echo $nm;

			?>

		</td>
		<td  style="background-color: #66ff99">
			<?php 

				$nm=$this->db->query("SELECT COUNT(b.id_transaksi) as j_kpb FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi JOIN tb_employee c on a.id_mekanik=c.id_employee JOIN tb_jasa d on b.id_jasa=d.id_jasa JOIN tb_group_jasa e on d.id_group_jasa=e.id_group_jasa where e.kode='OR' and c.id_employee='".$key['id_employee']."'")->row()->j_kpb;
				echo $nm;

			?>

		</td>
	</tr>
<?php endforeach; ?>
	
</table>
