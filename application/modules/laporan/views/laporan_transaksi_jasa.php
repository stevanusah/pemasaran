<style>
  .tables{
	  
      font-size:13px;
	  border-collapse: collapse;
      width: 100%;
	  height:100%;
      margin: 0 auto;
  }
  .tables th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: center;
  }
  .tables td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
  }
  </style>
  <?php $this->load->view('laporan/headercetak.php'); ?>
  <h3 style="text-align:center"><center>Laporan Transaksi Jasa</center></h3>
  <h4 style="text-align:center"><center>Periode (<?php echo $awal;?> s.d. <?php echo $akhir;?>)</center></h4>
  <br>
  <table class="tables">
              
            <thead>
              <tr>
                <th class="header" style="width: auto;">No</th>
                
                    <th style="width: auto;">No Transaksi</th> 
                
                    <th style="width: auto;">Deskripsi</th>    
                
                    <th style="width: auto;">Harga</th>   
                    
                    <th style="width: auto;">Total</th> 

                    <th style="width: auto;">Diskon</th> 

                    <th style="width: auto;">Sub Total</th> 

              </tr>
            </thead>
            
            
            <tbody>
                
               <?php 
               $qty=0;
               $gtotal=0;
               $jm=0;$tl=0; foreach ($transaksis as $data) :
               $total = ($data['harga_jasa']*($data['qty_jasa']));

               ?>
              <tr>
              	<td><?php echo $number++;; ?> </td>
               
                <td><?php echo $data['id_transaksi']; ?></td>

                <td><?php echo $data['jasa']; ?></td>

                <td><?php echo "Rp. ".number_format($data['harga_jasa'],0,'.',',')."" ?></td>

              
                <td><?php echo "Rp. ".number_format($total,0,'.',',')."" ?></td>


                <td><?php echo "Rp. ".number_format($data['diskon'],0,'.',',')."" ?></td>

                <?php $gt = $total - $data['diskon']; ?>

                <td><?php echo "Rp. ".number_format($gt,0,'.',',')."" ?></td>

                <?php $qty=$qty+$data['qty_jasa']; 
                      $gtotal=$gtotal+$gt;
                ?>

              <?php endforeach; ?>

                <tr>
                  <td rowspan="2" colspan="6" align="right"></td>
                  <td align="center"><b>Grand Total</b></td>
                </tr>
                <tr>
                  <td><b><?php echo 'Rp. '.number_format($gtotal,0,'.',',');?></b></td>
               </tr>
               
               
            </tbody>
          </table>