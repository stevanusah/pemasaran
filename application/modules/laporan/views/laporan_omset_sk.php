<?php if(!defined('BASEPATH'))exit('Tidak Punya akses');

header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename=Laporan Omset Perbulan.xls'); //tell browser what's the file nam
header('Cache-Control: max-age=0');
?>
<h2>LAPORAN OMSET SUKUCADANG</h2>
<h3>Laporan Omset Sukucadang Bulan : <?php echo $awal;?> s.d <?php echo $akhir?></h3>
<br>


<table border="1">
	<!-- Header -->
	<tr>
		<th style="background-color: #ffc000">Tanggal</th>
		<th style="background-color: #ffc000">OLI</th>
		<th style="background-color: #ffc000">BUSI</th>
		<th style="background-color: #ffc000">PENBELT</th>
		<th style="background-color: #ffc000">BAN</th>
		<th style="background-color: #ffc000">KAMPAS REM</th>
		<th style="background-color: #ffc000">SUKUCADANG</th>
		<th style="background-color: #ffc000">Total EDC MANDIRI</th>
		<th style="background-color: #ffc000">EDC BCA</th>
		<th style="background-color: #ffc000">CASH</th>
		<th style="background-color: #ffc000">TOTAL</th>

	</tr>

	<!-- Body -->
	<?php foreach ($transaksis as $key ) : ?>
	<tr>
		<td><?php echo $key['tanggal_transaksi']; ?></td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang where d.group_sukucadang='OLI' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nm != NULL) {
									echo $nm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang where d.group_sukucadang='BUSI' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nm != NULL) {
					echo $nm;			
				}else{
					echo "0";
				}
			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang where d.group_sukucadang='PENBELT' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nm != NULL) {
					echo $nm;			
				}else{
					echo "0";
				}

			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang where d.group_sukucadang='BAN' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nm != NULL) {
					echo $nm;			
				}else{
					echo "0";
				}

			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang where d.group_sukucadang='KAMPAS REM' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nm != NULL) {
					echo $nm;			
				}else{
					echo "0";
				}

			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang where d.group_sukucadang='SUKUCADANG' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nm != NULL) {
					echo $nm;			
				}else{
					echo "0";
				}

			?>
		</td>
		<td>
			<?php 

				$nmmandiri=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang join tb_pembayaran z on a.id_pembayaran=z.id_pembayaran where d.group_sukucadang='SUKUCADANG'and z.pembayaran = 'DEBIT MANDIRI' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nmmandiri != NULL) {
					echo $nmmandiri;			
				}else{
					echo "0";
				}

			?>
		</td>
		<td>
			<?php 

				$nmbca=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang join tb_pembayaran z on a.id_pembayaran=z.id_pembayaran where d.group_sukucadang='SUKUCADANG'and z.pembayaran = 'DEBIT BCA' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nmbca != NULL) {
					echo $nmbca;			
				}else{
					echo "0";
				}

			?>
		</td>
		<td>
			<?php 

				$nmcash=$this->db->query("select sum(b.harga_sukucadang) as total_oli FROM head_transaksi a join detail_transaksi_sukucadang b on a.id_transaksi=b.id_transaksi join tb_sukucadang c on b.id_sukucadang=c.id_sukucadang JOIN tb_group_sukucadang d on c.id_group_sukucadang=d.id_group_sukucadang join tb_pembayaran z on a.id_pembayaran=z.id_pembayaran where d.group_sukucadang='SUKUCADANG'and z.pembayaran = 'CASH' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_oli;
				if ($nmcash != NULL) {
					echo $nmcash;			
				}else{
					echo "0";
				}

			?>
		</td>
		<td>
			<?php

				$totalnm = $nmmandiri + $nmbca +$nmcash;
				echo $totalnm;

			?>
		</td>
	</tr>	
<?php endforeach; ?>
</table>
