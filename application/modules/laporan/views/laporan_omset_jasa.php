<?php if(!defined('BASEPATH'))exit('Tidak Punya akses');

header('Content-Type: application/vnd.ms-excel'); //mime type
header('Content-Disposition: attachment;filename=Laporan Omset Jasa.xls'); //tell browser what's the file nam
header('Cache-Control: max-age=0');
?>
<h2>LAPORAN OMSET JASA</h2>
<h3>Laporan Omset Jasa Bulan : <?php echo $awal;?> s.d <?php echo $akhir?></h3>

<br>


<table border="1">
	<!-- Header -->
	<tr>
		<th style="background-color: #ffc000">Tanggal</th>
		<th style="background-color: #ffc000">CS</th>
		<th style="background-color: #ffc000">LS</th>
		<th style="background-color: #ffc000">LR</th>
		<th style="background-color: #ffc000">HR</th>
		<th style="background-color: #ffc000">PIT EXPRESS</th>
		<th style="background-color: #ffc000">SERVICE KUNJUNGAN</th>
		<th style="background-color: #ffc000">CLAIM</th>
		<th style="background-color: #ffc000">Total EDC MANDIRI</th>
		<th style="background-color: #ffc000">EDC BCA</th>
		<th style="background-color: #ffc000">CASH</th>
		<th style="background-color: #ffc000">TOTAL</th>

	</tr>

	<!-- Body -->
	<?php foreach ($transaksis as $key) : ?>
	<tr>
		<td><?php echo $key['tanggal_transaksi'] ?></td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_jasa) as total_CS FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa where d.kode='CS' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_CS;
				if ($nm != NULL) {
									echo $nm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_jasa) as total_LS FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa where d.kode='LS' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_LS;
				if ($nm != NULL) {
									echo $nm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_jasa) as total_LR FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa where d.kode='LR' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_LR;
				if ($nm != NULL) {
									echo $nm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_jasa) as total_SE FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa where d.kode='HR' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_SE;
				if ($nm != NULL) {
									echo $nm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
			<?php 

				$nm=$this->db->query("select sum(b.harga_jasa) as total_SK FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa where d.kode='PE' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_SK;
				if ($nm != NULL) {
									echo $nm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
		<?php 

			$nm=$this->db->query("select sum(b.harga_jasa) as total_SK FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa where d.kode='SE' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_SK;
			if ($nm != NULL) {
								echo $nm;			
							}else{
								echo "0";
							}

		?>
		</td>
		<td><?php echo "0"; ?></td>
		<td>
			<?php 

				$mandirinm=$this->db->query("select sum(b.harga_jasa) as total_SK FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa join tb_pembayaran z on a.id_pembayaran=z.id_pembayaran where z.pembayaran='DEBIT MANDIRI' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_SK;
				if ($mandirinm != NULL) {
									echo $mandirinm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
			<?php 

				$bcanm=$this->db->query("select sum(b.harga_jasa) as total_SK FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa join tb_pembayaran z on a.id_pembayaran=z.id_pembayaran where z.pembayaran='DEBIT BCA' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_SK;
				if ($bcanm != NULL) {
									echo $bcanm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
			<?php 

				$cashnm=$this->db->query("select sum(b.harga_jasa) as total_SK FROM head_transaksi a join detail_transaksi_jasa b on a.id_transaksi=b.id_transaksi join tb_jasa c on b.id_jasa=c.id_jasa JOIN tb_group_jasa d on c.id_group_jasa=d.id_group_jasa join tb_pembayaran z on a.id_pembayaran=z.id_pembayaran where z.pembayaran='CASH' and a.tanggal_transaksi='".$key['tanggal_transaksi']."'")->row()->total_SK;
				if ($cashnm != NULL) {
									echo $cashnm;			
								}else{
									echo "0";
								}

			?>
		</td>
		<td>
			<?php $totals = $mandirinm + $bcanm + $cashnm;
			echo $totals;
			?>

		</td>
	</tr>	
<?php endforeach; ?>
</table>
