<style>
  .tables{
	  
      font-size:13px;
	  border-collapse: collapse;
      width: 100%;
	  height:100%;
      margin: 0 auto;
  }
  .tables th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: center;
  }
  .tables td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
  }
  </style>
  <?php $this->load->view('laporan/headercetak.php'); ?>
  <h3 style="text-align:center"><center>Laporan Transaksi dengan No.<?= $transaksi['id_transaksi'] ?></center></h3>
  
<br>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <table>
        <tbody>
        
          <tr>
            <td width="200"><b>No Transaksi</td>
            <td><?= $transaksi['id_transaksi'] ?></td>
            
          </tr>
          <tr>
            <td width="200"><b>Tanggal Transaksi</td>
            <td><?= $transaksi['tanggal_transaksi'] ?></td>
            
          </tr>

          <tr>
            <td width="200"><b>Nama Konsumen</td>
            <td><?= $transaksi['nama'] ?></td>
            
          </tr>

          <tr>
            <td width="200"><b>Nama Mekanik</td>
            <td><?= $transaksi['name'] ?></td>
            
          </tr>

          <tr>
            <td width="200"><b>Kondisi Kendaraan</td>
            <td><?= $transaksi['kondisi_kendaraan'] ?></td>
            
          </tr>
          
        
        </tbody>
      </table>


      <h3 style="text-align:center"><b>Data Jasa yang digunakan</b></h3>

      <table class="tables table-hover table-condensed">
              
            <thead>
              <tr>
                <th class="header">No</th>
                    <th>Kode Jasa</th>  
                    <th>Nama Jasa</th> 
                    <th>Harga</th>  
                    <th>QTY</th>  
                    <th>Total</th>
                    <th>Diskon</th>
                    <th>Sub Total</th>
              </tr>
            </thead>
            <tbody>
              <?php $no =1; ?>
              <?php $hasill=0; ?>
              <?php $totall=0; ?>
              <?php foreach ($transjasa as $key) : ?>
              <tr>
              
               <td><?= $no++; ?></td>
               
               <td><?= $key['id_jasa'] ?></td>

               <td><?= $key['jasa'] ?></td>

               <td><?= number_format($key['harga_jasa']) ?></td>

               <td><?= $key['qty_jasa'] ?></td>


               <?php $totall =  $key['qty_jasa'] * $key['harga_jasa']  ?>  
               <td><?= number_format($totall)  ?></td>

               <td><?= number_format($key['diskon'])  ?></td>

               <?php $st = $totall - $key['diskon']; ?>

               <td><?= number_format($st);  ?></td>

               <?php $hasill = $hasill + $st; ?>
              
                </tr>
              
            
          <?php endforeach; ?>

          <tr>
                <th colspan="7">Sub Total</th>
                <th><?= number_format($hasill) ?></th>
              </tr>
          </table>

          <h3 style="text-align:center"><b>Data Sukucadang yang digunakan</b></h3>

      <table class="tables table-hover table-condensed">
              
            <thead>
              <tr>
                <th class="header">No</th>
                    <th>Kode Sukucadang</th>  
                    <th>Nama Sukucadang</th> 
                    <th>Harga</th>  
                    <th>QTY</th>  
                    <th>Total</th>

                    <th>Diskon</th>
                    <th>Sub Total</th>
              </tr>
            </thead>
            <tbody>
              <?php $no =1; ?>
              <?php $hasil =0; ?>
              <?php foreach ($transsk as $key) : ?>
              <tr>
              
               <td><?= $no++; ?></td>
               
               <td><?= $key['id_sukucadang'] ?></td>

               <td><?= $key['sukucadang'] ?></td>

               <td><?= number_format($key['harga_sukucadang']) ?></td>

               <td><?= $key['qty_sukucadang'] ?></td>

              <?php $total =  $key['qty_sukucadang'] * $key['harga_sukucadang']  ?>

               <td><?= number_format($total); ?></td>


               <td><?= number_format($key['diskon'])  ?></td>

               <?php $stt = $total - $key['diskon']; ?>

               <td><?= number_format($stt);  ?></td>

               <?php $hasil = $hasil + $stt ?>
              </tr>  
          <?php endforeach; ?>
          <tr>
                <th colspan="7">Sub Total</th>
                <th><?= number_format ($hasil); ?></th>
              </tr>
          </table>
          <label><b>Grand Total</b></label>
          <label style="margin-left: 10px">:</label>
          <?php $gt = $hasil + $hasill ?>
          <label style="margin-left: 600px"><b>Rp. <?php echo number_format($gt); ?></b></label></br>

    </div>
  </div>
</div>
  <!-- <table class="tables table-striped table-hover">
        <tbody>
        
          <tr>
            <td style="width: auto;"><b>No Transaksi</td>
            <td><?= $transaksi['id_transaksi'] ?></td>
            
          </tr>
          <tr>
            <td style="width: auto;"><b>Tanggal Transaksi</td>
            <td><?= $transaksi['tanggal_transaksi'] ?></td>
            
          </tr>

          <tr>
            <td style="width: auto;"><b>Nama Konsumen</td>
            <td><?= $transaksi['nama'] ?></td>
            
          </tr>

          <tr>
            <td style="width: auto;"><b>Nama Mekanik</td>
            <td><?= $transaksi['name'] ?></td>
            
          </tr>

          <tr>
            <td style="width: auto;"><b>Kondisi Kendaraan</td>
            <td><?= $transaksi['kondisi_kendaraan'] ?></td>
            
          </tr>
          
        
        </tbody>
      </table>
      <table>
            <thead>
              <tr>
                <th class="header" style="width: auto;">No</th>
                
                    <th style="width: auto;">Kode Jasa</th>   
                
                    <th style="width: auto;">Nama Jasa</th>   
                
                    <th style="width: auto;">Harga</th>    
                
                    <th style="width: auto;">QTY</th>

                    <th style="width: auto;">Total</th>      

              </tr>
            </thead>
            
            
            <tbody>
                
               <?php 
               $total=0; foreach ($transjasa as $data) :
               ?>
              <tr>
              	<td align="center"><?php echo $number++;; ?> </td>
               
                <td><?php echo $data['id_jasa']; ?></td>
               
                <td><?php echo $data['jasa']; ?></td>

                <td><?php echo "Rp. ".number_format($data['harga'],0,'.',',')."" ?></td>

                <td><?php echo $data['qty_jasa']; ?></td>

                <?php  
                  $gtotal=$gtotal+$data['harga'];
                ?>

              <?php endforeach; ?>

                <tr>
                  <td colspan="4" align="center"><b>Sub Total</b></td>
                  <td><b><?php echo 'Rp. '.number_format($gtotal,0,'.',',');?></b></td>
               </tr>
               
               
            </tbody>
          </table> -->