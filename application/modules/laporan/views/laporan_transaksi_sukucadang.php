<style>
  .tables{
	  
      font-size:13px;
	  border-collapse: collapse;
      width: 100%;
	  height:100%;
      margin: 0 auto;
  }
  .tables th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: center;
  }
  .tables td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
  }
  </style>
  <?php $this->load->view('laporan/headercetak.php'); ?>
  <h3 style="text-align:center"><center>Laporan Transaksi Sparepart</center></h3>
  <h4 style="text-align:center"><center>Periode (<?php echo $awal;?> s.d. <?php echo $akhir;?>)</center></h4>
  <br>
  <table class="tables">
              
            <thead>
              <tr>
                <th class="header" style="width: auto;">No</th>
                
                    <th style="width: auto;">No Transaksi</th>   
                
                    <th style="width: auto;">Kode Sukucadang</th>   
                
                    <th style="width: auto;">Deskripsi</th>    
                
                    <th style="width: auto;">Harga</th>   
                    
                    <th style="width: auto;">Total</th> 

                     <th style="width: auto;">Diskon</th> 

                      <th style="width: auto;">SubTotal</th> 

              </tr>
            </thead>
            
            
            <tbody>
                
               <?php 
               $qty=0;
               $gtotal=0;
               $jm=0;$tl=0; foreach ($transaksis as $data) :
               $total = ($data['harga']*($data['qty_sukucadang']));

               ?>
              <tr>
              	<td><?php echo $number++;; ?> </td>
               
                <td><?php echo $data['id_transaksi']; ?></td>
               
                <td><?php echo $data['id_sukucadang']; ?></td>

                <td><?php echo $data['sukucadang']; ?></td>

                <td><?php echo "Rp. ".number_format($data['harga'],0,'.',',')."" ?></td>

                <td><?php echo "Rp. ".number_format($data['harga'],0,'.',',')."" ?></td>

                <td><?php echo "Rp. ".number_format($data['diskon'],0,'.',',')."" ?></td> 
                <?php $gt = $total - $data['diskon'];  ?> 

                <td><?php echo "Rp. ".number_format($gt,0,'.',',')."" ?></td>
                <?php $qty=$qty+$data['qty_sukucadang']; 
                      $gtotal=$gtotal+$gt;
                ?>



              <?php endforeach; ?>

                <tr>
                  <td rowspan="2" colspan="7" align="right"></td>
                  
                  <td align="center"><b>Grand Total</b></td>
                </tr>
                <tr>
                  
                  <td><b><?php echo 'Rp. '.number_format($gtotal,0,'.',',');?></b></td>
               </tr>
               
               
            </tbody>
          </table>