<style>
  .tables{
    
      font-size:13px;
    border-collapse: collapse;
      width: 100%;
    height:100%;
      margin: 0 auto;
  }
  .tables th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: center;
  }
  .tables td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
  }
  </style>
  <?php $this->load->view('laporan/headercetak.php'); ?>
  <h3 style="text-align:center"><center>Laporan Work Order</center></h3>
  
  <table class="tables">
              
            <thead>
              <tr>
                <th class="header" style="width: auto;">No</th>
                
                    <th style="width: auto;">No Transaksi</th>   
                
                    <th style="width: auto;">Tanggal Transaksi</th>   
                
                    <th style="width: auto;">Konsumen</th>    
                
                    <th style="width: auto;">Mekanik</th>   
                
                    <th style="width: auto;">Total Jasa</th> 
                    
                    <th style="width: auto;">Total Sukucadang</th> 

                    <th style="width: auto;">Total Service</th> 

              </tr>
            </thead>
            
            
            <tbody>
                
               <?php 
               $number = 1;
               $totals = 0;
               foreach ($transaksis as $data) :
                 

               
               ?>
              <tr>
                <td><?php echo $number++; ?> </td>
               
                <td><?= $data['id_transaksi'] ?></td>
               
                <td><?= $data['tanggal_transaksi'] ?></td>

                <td><?= $data['nama'] ?></td>

                <td><?= $data['name'] ?></td>

                <td>
                  <?php 
                  $total_j=$this->db->query("SELECT id_transaksi,sum(harga_jasa*qty_jasa - diskon) as total_jasa from detail_transaksi_jasa where id_transaksi='".$data['id_transaksi']."' GROUP BY id_transaksi")->row()->total_jasa;
                
                echo number_format($total_j) ;

                ?></td>

                <td>
                <?php
                $total_s=$this->db->query("SELECT id_transaksi,sum(harga_sukucadang*qty_sukucadang - diskon) as total_sc from detail_transaksi_sukucadang where id_transaksi='".$data['id_transaksi']."' GROUP by id_transaksi")->row()->total_sc;

                echo number_format($total_s);
                ?>
                  
                </td>

                <td>
                  <?=number_format($total_j + $total_s) ?></td>

                <?php $totals = $totals+($total_j + $total_s); ?>

              </tr>
              
              <?php endforeach; ?>
              <tr>
                  <td colspan="7"><b>Total</b></td>
                 <td><b><?= number_format($totals); ?></b></td>
              </tr>

            </tbody>
          </table>