<style>
  .tables{
	  
      font-size:13px;
	  border-collapse: collapse;
      width: 100%;
	  height:100%;
      margin: 0 auto;
  }
  .tables th{
      border:1px solid #000;
      padding: 3px;
      font-weight: bold;
      text-align: center;
  }
  .tables td{
      border:1px solid #000;
      padding: 3px;
      vertical-align: top;
  }
  </style>
  <?php $this->load->view('laporan/headercetak.php'); ?>
  <h3 style="text-align:center"><center>Laporan Transaksi Selling Sukucadang</center></h3>
  <h4 style="text-align:center"><center>Periode (<?php echo $awal;?> s.d. <?php echo $akhir;?>)</center></h4>
  <br>
  <table class="tables">
              
            <thead>
              <tr>
                <th class="header" style="width: auto;">No</th>
                
                    <th style="width: auto;">No Transaksi</th>   
                
                    <th style="width: auto;">Kode Sukucadang</th>   
                
                    <th style="width: auto;">Deskripsi</th>    
                
                    <th style="width: auto;">Harga</th>   
                
                    <th style="width: auto;" align="center">QTY</th> 
                    
                    <th style="width: auto;">Total</th> 

              </tr>
            </thead>
            
            
            <tbody>
                
               <?php 
               $qty=0;
               $gtotal=0;
               $jm=0;$tl=0; foreach ($transaksis as $data) :
               $total = ($data['harga_sukucadang']*($data['qty_sukucadang']));

               ?>
              <tr>
              	<td><?php echo $number++;; ?> </td>
               
                <td><?php echo $data['id_transaksi_direct']; ?></td>
               
                <td><?php echo $data['id_sukucadang']; ?></td>

                <td><?php echo $data['sukucadang']; ?></td>

                <td><?php echo "Rp. ".number_format($data['harga_sukucadang'],0,'.',',')."" ?></td>

                <td align="center"><?php echo $data['qty_sukucadang']; ?></td>

                <td><?php echo "Rp. ".number_format($total,0,'.',',')."" ?></td>
                <?php $qty=$qty+$data['qty_sukucadang']; 
                      $gtotal=$gtotal+$total;
                ?>

              <?php endforeach; ?>

                <tr>
                  <td rowspan="2" colspan="5" align="right"></td>
                  <td align="center" style="width: auto;"><b>Total QTY</b></td>
                  <td align="center" style="width: auto;"><b>Grand Total</b></td>
                </tr>
                <tr>
                  <td align="center" style="width: auto;"><b><?php echo $qty;?></b></td>
                  <td align="center" style="width: auto;"><b><?php echo 'Rp. '.number_format($gtotal,0,'.',',');?></b></td>
               </tr>
               
               
            </tbody>
          </table>