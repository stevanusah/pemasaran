<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of katalog_pisau
 * @created on : Saturday, 26-Aug-2017 07:19:12
 * @author DAUD D. SIMBOLON <daud.simbolon@gmail.com>
 * Copyright 2017    
 */
 
 
class laporans extends CI_Model 
{
 
    public function __construct() 
    {
        parent::__construct();
    }


    /**
     *  Get All data katalog_pisau
     *
     *  @param limit  : Integer
     *  @param offset : Integer
     *
     *  @return array
     *
     */
	 public function get_data_sukucadang($awal,$akhir) 
    {

         $result = $this->db->query( 
            "SELECT a.*,b.*,c.* from head_transaksi a 
            join detail_transaksi_sukucadang b on a.id_transaksi = b.id_transaksi
            join tb_sukucadang c on b.id_sukucadang = c.id_sukucadang
            where a.tanggal_transaksi BETWEEN '".$awal."' and '".$akhir."' order by tanggal_transaksi asc");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }
    public function get_data_insentif($awal,$akhir,$mekanik) 
    {

         $result = $this->db->query( 

            "SELECT *, b.nilai_jasa*b.persentase/100 as insentif,b.persentase as permek FROM head_transaksi a JOIN detail_transaksi_jasa b on a.id_transaksi = b.id_transaksi join
            tb_employee c on a.id_mekanik = c.id_employee JOIN tb_jasa d on b.id_jasa = d.id_jasa join tb_group_jasa e on d.id_group_jasa=e.id_group_jasa join tb_konsumen f on a.id_konsumen = f.nopol WHERE
            id_mekanik='".$mekanik."' and tanggal_transaksi BETWEEN '".$awal."' and '".$akhir."' order by tanggal_transaksi asc");
            

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    public function get_data_jasa($awal,$akhir) 
    {

         $result = $this->db->query( 
            "SELECT a.*,b.*,c.* from head_transaksi a 
            join detail_transaksi_jasa b 
            on a.id_transaksi = b.id_transaksi
            join tb_jasa c 
            on b.id_jasa = c.id_jasa
            where a.tanggal_transaksi BETWEEN '".$awal."' and '".$akhir."' order by tanggal_transaksi asc");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }


    public function get_workorder_all(){

         $result = $this->db->query( 
            "SELECT a.*,b.*,c.* FROM head_transaksi a
            JOIN tb_konsumen b on a.id_konsumen = b.nopol join tb_employee c on a.id_mekanik=c.id_employee");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }

    }

    // public function get_workorder_all_detail(){

    //      $result = $this->db->query( 
    //         "SELECT id_transaksi,sum(harga_jasa*qty_jasa - diskon) as total_jasa from detail_transaksi_jasa GROUP BY id_transaksi ");

    //     if ($result->num_rows() > 0) 
    //     {
    //         return $result->result_array();
    //     } 
    //     else 
    //     {
    //         return array();
    //     }

    // }

    //  public function get_workorder_all_detail_sc(){

    //      $result = $this->db->query( 
    //         "SELECT id_transaksi,sum(harga_sukucadang*qty_sukucadang - diskon) as total_sc from detail_transaksi_sukucadang GROUP by id_transaksi");

    //     if ($result->num_rows() > 0) 
    //     {
    //         return $result->result_array();
    //     } 
    //     else 
    //     {
    //         return array();
    //     }

    // }

    public function get_selling_sukucadang_all()
     {

         $result = $this->db->query( 
            "SELECT a.*,b.*,c.* FROM tb_sukucadang a
            JOIN tb_group_sukucadang b on a.id_group_sukucadang = b.id_group_sukucadang join detail_direct_sukucadang c on a.id_sukucadang=c.id_sukucadang");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    public function get_jasa() 
    {

         $result = $this->db->query( 
            "SELECT a.*,b.* FROM tb_jasa a
            JOIN tb_group_jasa b on a.id_group_jasa = b.id_group_jasa");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    public function get_sukucadang() 
    {

         $result = $this->db->query( 
            "SELECT a.*,b.* FROM tb_sukucadang a
            JOIN tb_group_sukucadang b on a.id_group_sukucadang = b.id_group_sukucadang");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }
    public function get_mekanik()
    {       
        $this->db->SELECT('*');
        $this->db->from('tb_employee');

        $result = $this->db->get();
        return $result->result();
        
        
    }

    //baru create by chandro
     public function get_data_selling_jasa($awal,$akhir) 
    {

         $result = $this->db->query( 
            "SELECT a.*,b.*,c.* from head_direct a 
            join detail_direct_jasa b 
            on a.id_transaksi_direct = b.id_transaksi_direct
            join tb_jasa c 
            on b.id_jasa = c.id_jasa
            where a.tanggal_transaksi_direct BETWEEN '".$awal."' and '".$akhir."' order by tanggal_transaksi_direct asc");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

     //baru create by chandro
     public function get_data_selling_sukucadang($awal,$akhir) 
    {

         $result = $this->db->query( 
            "SELECT a.*,b.*,c.* from head_direct a 
            join detail_direct_sukucadang b 
            on a.id_transaksi_direct = b.id_transaksi_direct
            join tb_sukucadang c 
            on b.id_sukucadang = c.id_sukucadang
            where a.tanggal_transaksi_direct BETWEEN '".$awal."' and '".$akhir."' order by tanggal_transaksi_direct asc");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    public function get_wo_periode($awal,$akhir) 
    {

         $result = $this->db->query( 
            "SELECT a.*,b.*,c.* FROM head_transaksi a
            JOIN tb_konsumen b on a.id_konsumen = b.nopol join tb_employee c on a.id_mekanik=c.id_employee
            where a.tanggal_transaksi BETWEEN '".$awal."' and '".$akhir."' order by tanggal_transaksi asc");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    public function get_jasa_trans($id){

        $this->db->select('*');
        $this->db->from('head_transaksi a');
        $this->db->join('detail_transaksi_jasa b','a.id_transaksi=b.id_transaksi');
        $this->db->join('tb_jasa c','c.id_jasa=b.id_jasa');

        $this->db->where('a.id_transaksi', $id);
         $result = $this->db->get();

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }

    }

    public function get_sukucadang_trans($id){

        $this->db->select('*');
        $this->db->from('head_transaksi a');
        $this->db->join('detail_transaksi_sukucadang b','a.id_transaksi=b.id_transaksi');
        $this->db->join('tb_sukucadang c','b.id_sukucadang=c.id_sukucadang');
        $this->db->where('a.id_transaksi', $id);
         $result = $this->db->get();

        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else
        {
            return array();
        }

    }

    public function get_one_trans($id){

        $result = $this->db->query( 
            "SELECT * FROM head_transaksi a
            JOIN tb_employee b ON a.id_mekanik = b.id_employee
            JOIN tb_konsumen c ON a.id_konsumen = c.nopol
            WHERE a.id_transaksi = '".$id."'");

        if ($result->num_rows() > 0) 
        {
            return $result->row_array();
        } 
        else 
        {
            return array();
        }

    }

    public function getListMotor(){

        $result = $this->db->query("SELECT * from tb_motor");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }

    }

    public function getlistSukucadang(){

        $result = $this->db->query("SELECT * FROM tb_group_sukucadang");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }

    }

     public function getListMekanik(){

        $result = $this->db->query("SELECT * from tb_employee");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }

    }

    // Coding Amf
    // Create date :15/03/2018
    public function get_omset_sk_periode($awal,$akhir) 
    {

         $result = $this->db->query( 
            "SELECT * FROM head_transaksi a
            JOIN detail_transaksi_sukucadang b on a.id_transaksi = b.id_transaksi 
            where a.tanggal_transaksi BETWEEN '".$awal."' and '".$akhir."' GROUP by tanggal_transaksi asc ");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    public function get_omset_jasa_periode($awal,$akhir) 
    {

         $result = $this->db->query( 
            "SELECT * FROM head_transaksi a
            JOIN detail_transaksi_jasa b on a.id_transaksi = b.id_transaksi 
           where a.tanggal_transaksi BETWEEN '".$awal."' and '".$akhir."' GROUP by tanggal_transaksi asc");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }

    public function get_lbb_periode($awal,$akhir) 
    {

         $result = $this->db->query( 
            "SELECT * FROM head_transaksi a
            JOIN detail_transaksi_sukucadang b on a.id_transaksi = b.id_transaksi 
            where a.tanggal_transaksi BETWEEN '".$awal."' and '".$akhir."' order by tanggal_transaksi asc");

        if ($result->num_rows() > 0) 
        {
            return $result->result_array();
        } 
        else 
        {
            return array();
        }
    }
	


}
