</div><!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">

    </div>
    <strong>IDNETSOLUTION &copy; 2017 </strong> All rights reserved.
</footer>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.3 -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jQuery/jQuery-2.1.3.min.js') ?>"></script>
<script src='<?php echo base_url('assets/adminlte/js/select2.min.js') ?>'></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>

<!-- morris -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/morris/raphael-min.js');?>"></script>
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/morris/morris.min.js');?>"></script>

<!-- SlimScroll -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/slimScroll/jquery.slimScroll.min.js') ?>" type="text/javascript"></script>
<!-- FastClick -->
<script src='<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/fastclick/fastclick.min.js') ?>'></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/dist/js/app.min.js') ?>" type="text/javascript"></script>
<script src='<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/datatables/jquery.dataTables.min.js') ?>'></script>

<script src='<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/datatables/dataTables.bootstrap.min.js') ?>'></script>

<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jquery-bracket-master/dist/jquery.bracket.min.js')?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/parsley/parsley.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/parsley/i18n/id.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jQuery-Brackets-Bracket-World/dist/assets/scripts/jquery.bracket-world.min.js'); ?>"></script>

<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/datepicker/bootstrap-datepicker.js')?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/timepicker/bootstrap-timepicker.min.js')?>" type="text/javascript"></script>

<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/bower_components/morris.js/morris.min.js')?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')?>"></script>
<!-- jvectormap -->
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')?>"></script>
<script src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.example1').DataTable();
	});

	$(function() {
    $( "#tgl" ).datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		endDate: 'y',
		// startDate: 'y',


		});
  	});

    $(function() {
    $( ".akhir" ).datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		});
  	});

  	$(function() {
    $( "#tgl2" ).datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		endDate: '-15y',


		});
  	});

	$(function() {
    $( "#thn" ).datepicker({
		autoclose: true,
		format: "yyyy",
    	viewMode: "years",
    	minViewMode: "years",
		startDate: '-7y',
		endDate: 'y'


		});
  	});

	$(function() {
    $( "#akhir_periode" ).datepicker({

		autoclose: true,
		format: "yyyy-mm-dd",
		startDate: '+70d',
		endDate: '+7m'


		});
  	});

	$(function() {
    $( "#akhir_pendaftaran" ).datepicker({

		autoclose: true,
		format: "yyyy-mm-dd",
		startDate: '+7d',
		endDate: '+2m'


		});
  	});

	$(".timepicker").timepicker({
      showInputs: false,
	  showMeridian :false,
	  defaultTime : false,
	  minuteStep : 5

    });

  $(".nonaktif").keydown(function(event) {
    return false;
});

function angka(a){
	var charCode = (a.which) ? a.which : event.keyCode
	if (charCode > 31 && (charCode <48 || charCode >57))
		return false;
	return true;
}


function limit_checkbox(max,identifier)
{
	var checkbox = $("input[name='"+identifier+"[]']");
	var checked  = $("input[name='"+identifier+"[]']:checked").length;
	checkbox.filter(':not(:checked)').prop('disabled', checked >= max);

}

 // New Code ~23/12/2017~
 $(document).ready(function () {
   $(".pilih").select2({
     placeholder: "Please Select"
   });
 });

 $(document).ready(function () {
   $("#kode_jasa").select2({
     placeholder: "Please Select"
   });
 });
 $(document).ready(function () {
   $("#kode_sukucadang").select2({
     placeholder: "Please Select"
   });
 });

 function angka(a) {
   if (!/^[0-9]+$/.test(a.value)) {
     a.value = a.value.substring(0,a.value.length-20);
   }
 }
</script>

<script type="text/javascript">
      $(function () {
        "use strict";

        // AREA CHART
        
        // LINE CHART
        var months = ["Jan", "Feb", "Mar" , "Apr", "Mei", "Jun", "Jul", "Agu", "Sep","Okt","Nov","Des"];
        var line = new Morris.Line({
          element: 'line-chart',
          //resize: true,
          data: [
            {y: '<?php echo date('Y');?>-1', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-01-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-2', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-02-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-3', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-03-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-4', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-04-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-5', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-05-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-6', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-06-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-7', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-07-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-8', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-08-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-9', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-09-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-10', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-10-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-11', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-11-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-12', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi) as jml FROM `head_transaksi` WHERE tanggal_transaksi like '%-12-%'")->jml;?>}
          ],
          xkey: 'y',
          ykeys: ['item1'],
          labels: ['Total'],
          lineColors: ['#3c8dbc'],
          xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
          var month = months[x.getMonth()];
          return month;
          },
          dateFormat: function(x) {
            var month = months[new Date(x).getMonth()];
            return month;
          },
          //hideHover: 'auto'
        });

        //DONUT CHART
        
        //BAR CHART
          
      });
    </script> 	

    <script type="text/javascript">
      $(function () {
        "use strict";

        // AREA CHART
        
        // LINE CHART
        var months = ["Jan", "Feb", "Mar" , "Apr", "Mei", "Jun", "Jul", "Agu", "Sep","Okt","Nov","Des"];
        var line = new Morris.Line({
          element: 'line-chart2',
          //resize: true,
          data: [
            {y: '<?php echo date('Y');?>-1', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-01-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-2', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-02-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-3', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-03-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-4', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-04-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-5', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-05-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-6', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-06-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-7', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-07-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-8', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-08-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-9', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-09-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-10', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-10-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-11', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-11-%'")->jml;?>},
            {y: '<?php echo date('Y');?>-12', item1: <?php echo $this->global_m->get_get("SELECT count(id_transaksi_direct) as jml FROM `head_direct` WHERE tanggal_transaksi_direct like '%-12-%'")->jml;?>}
          ],
          xkey: 'y',
          ykeys: ['item1'],
          labels: ['Total'],
          lineColors: ['#3c8dbc'],
          xLabelFormat: function(x) { // <--- x.getMonth() returns valid index
          var month = months[x.getMonth()];
          return month;
          },
          dateFormat: function(x) {
            var month = months[new Date(x).getMonth()];
            return month;
          },
          //hideHover: 'auto'
        });

        //DONUT CHART
        
        //BAR CHART
          
      });
    </script>
    
