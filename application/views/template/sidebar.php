<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
      <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/adminlte/AdminLTE-2.0.5/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->userdata('namalengkap');?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form --><!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <?php if ($this->session->userdata('akses') == "1") { ?>
            <li class="header">MENU APLIKASI</li>
            <li class="treeview">
                <a href="<?php echo site_url('dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Home</span></a></li>

            <li class="treeview">
                <a href="#">
                <i class="fa fa-database "></i> <span>Master Data</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('tb_konsumen');?>"><i class="fa fa-angle-double-down"></i>Data Konsumen</a></li>
                    <li><a href="<?php echo site_url('tb_employee');?>"><i class="fa fa-angle-double-down"></i>Data Karyawan</a></li>
                    <li><a href="<?php echo site_url('tb_operators');?>"><i class="fa fa-angle-double-down"></i>Data Operator</a></li>
                    <li><a href="<?php echo site_url('tb_group_jasa');?>"><i class="fa fa-angle-double-down"></i>Data Group Jasa</a></li>
                    <li><a href="<?php echo site_url('tb_group_sukucadang');?>"><i class="fa fa-angle-double-down"></i>Data Group Sukucadang</a></li>
                    <li><a href="<?php echo site_url('tb_jenis_motor');?>"><i class="fa fa-angle-double-down"></i>Data Jenis Motor</a></li>
                    <li><a href="<?php echo site_url('tb_jasa');?>"><i class="fa fa-angle-double-down"></i>Data Jasa</a></li>
                    <li><a href="<?php echo site_url('tb_sukucadang');?>"><i class="fa fa-angle-double-down"></i>Data Sukucadang</a></li>
                    <li><a href="<?php echo site_url('tb_motor');?>"><i class="fa fa-angle-double-down"></i>Data Motor</a></li>
                    <li><a href="<?php echo site_url('tb_pembayaran');?>"><i class="fa fa-angle-double-down"></i>Data Pembayaran</a></li>
                    <!-- <li><a href="<?php echo site_url('tb_presentase');?>"><i class="fa fa-angle-double-down"></i>Data Persentase</a></li> -->
                    
                </ul>
            </li>

            <li class="treeview">
                <a href="<?php echo site_url('transaksi');?>">
            <i class="fa fa-money"></i> <span>Work Order</span></a></li>

            <li class="treeview">
                <a href="<?php echo site_url('direct');?>">
            <i class="fa fa-money"></i> <span>Penjualan Sparepart</span></a></li>

            <li class="treeview">
                <a href="#">
                <i class="fa fa-print "></i> <span>Lap. Work Order</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo site_url('laporan/cetak_workorder_all');?>" target="_blank">
                        <i class="fa fa-circle-o"></i> <span>Lap. Work Order</span></a>
                    </li>
                    <li>
                    <a href="<?php echo site_url('laporan/get_wo_periode');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Work Order Periodik</span></a>
                </li>
                    <li>
                        <a href="<?php echo site_url('laporan/cetak_jasa');?>" target="_blank">
                        <i class="fa fa-circle-o"></i> <span>Lap. Jasa</span></a>
                    </li>

                    <li>
                        <a href="<?php echo site_url('laporan/cetak_sukucadang');?>" target="_blank">
                        <i class="fa fa-circle-o"></i> <span>Lap. Part</span></a>
                    </li>
                    
                    <li>
                        <a href="<?php echo site_url('laporan/get_sukucadang');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Transaksi Part</span></a>
                    </li>

                    <li>
                        <a href="<?php echo site_url('laporan/get_jasa');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Transaksi Jasa</span></a>
                    </li>

                    <li>
                        <a href="<?php echo site_url('laporan/get_insentif');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Insentif Mekanik</span></a>
                    </li>

                   <li>
                        <a href="<?php echo site_url('laporan/get_excel');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Lbb</span></a>
                    </li>

                    <li>
                        <a href="<?php echo site_url('laporan/get_lbb_periodik');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Lbb Periodik</span></a>
                    </li>
                    
                    <li>
                        <a href="<?php echo site_url('laporan/get_excel1');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Mekanik</span></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('laporan/get_omset_sk_periode');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Omset Sukucadang</span></a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('laporan/get_omset_jasa_periode');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Omset Jasa</span></a>
                    </li>
                </ul>
            </li>
             <li class="treeview">
                <a href="#">
                <i class="fa fa-print "></i> <span>Lap. Penjualan Sparepart</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    
                    
                    <li>
                        <a target="_blank" href="<?php echo site_url('laporan/cetak_transaksi_selling_part_all');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Tr Penjualan Part</span></a>
                    </li>

                    <li>
                        <a href="<?php echo site_url('laporan/get_selling_sukucadang');?>">
                        <i class="fa fa-circle-o"></i> <span>Lap. Tr Penjualan Part Periodik</span></a>
                    </li>

                   
                    
                </ul>
            </li>

            <li class="treeview">
                <a href="<?php echo site_url('login/logout');?>">
            <i class="fa fa-arrow-left"></i> <span>Logout</span></a></li>
      </ul>
      <?php } ?>
      <?php if ($this->session->userdata('akses') == "2" ) { ?>
         
        <li class="header">MENU APLIKASI</li>
        <li class="treeview">
            <a href="<?php echo site_url('dashboard');?>">
        <i class="fa fa-dashboard"></i> <span>Home</span></a></li>
        <li class="treeview">
            <a href="#">
            <i class="fa fa-database "></i> <span>Master Data</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <li><a href="<?php echo site_url('tb_konsumen');?>"><i class="fa fa-angle-double-down"></i>Data Konsumen</a></li>
                <!-- <li><a href="<?php echo site_url('tb_employee');?>"><i class="fa fa-angle-double-down"></i>Data Karyawan</a></li> -->
                <!-- <li><a href="<?php echo site_url('tb_operators');?>"><i class="fa fa-angle-double-down"></i>Data Operator</a></li> -->
                <li><a href="<?php echo site_url('tb_group_jasa');?>"><i class="fa fa-angle-double-down"></i>Data Group Jasa</a></li>
                <li><a href="<?php echo site_url('tb_group_sukucadang');?>"><i class="fa fa-angle-double-down"></i>Data Group Sukucadang</a></li>
                <li><a href="<?php echo site_url('tb_jenis_motor');?>"><i class="fa fa-angle-double-down"></i>Data Jenis Motor</a></li>
                <li><a href="<?php echo site_url('tb_jasa');?>"><i class="fa fa-angle-double-down"></i>Data Jasa</a></li>
                <li><a href="<?php echo site_url('tb_sukucadang');?>"><i class="fa fa-angle-double-down"></i>Data Sukucadang</a></li>
                <li><a href="<?php echo site_url('tb_motor');?>"><i class="fa fa-angle-double-down"></i>Data Motor</a></li>
                <li><a href="<?php echo site_url('tb_pembayaran');?>"><i class="fa fa-angle-double-down"></i>Data Pembayaran</a></li>
                
            </ul>
        </li>
        <li class="treeview">
            <a href="<?php echo site_url('transaksi');?>">
        <i class="fa fa-money"></i> <span>Work Order</span></a></li>

        <li class="treeview">
            <a href="<?php echo site_url('direct');?>">
        <i class="fa fa-money"></i> <span>Penjualan Sparepart</span></a></li>

        <li class="treeview">
            <a href="#">
            <i class="fa fa-print "></i> <span>Lap. Work Order</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                <li>
                    <a href="<?php echo site_url('laporan/cetak_workorder_all');?>" target="_blank">
                    <i class="fa fa-circle-o"></i> <span>Lap. Work Order</span></a>
                </li>
                 <li>
                    <a href="<?php echo site_url('laporan/cetak_workorder_periodik');?>" target="_blank">
                    <i class="fa fa-circle-o"></i> <span>Lap. Work Order Periodik</span></a>
                </li>
                <li>
                    <a href="<?php echo site_url('laporan/cetak_jasa');?>" target="_blank">
                    <i class="fa fa-circle-o"></i> <span>Lap. Jasa</span></a>
                </li>

                <li>
                    <a href="<?php echo site_url('laporan/cetak_sukucadang');?>" target="_blank">
                    <i class="fa fa-circle-o"></i> <span>Lap. Part</span></a>
                </li>
                
                <li>
                    <a href="<?php echo site_url('laporan/get_sukucadang');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Transaksi Part</span></a>
                </li>

                <li>
                    <a href="<?php echo site_url('laporan/get_jasa');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Transaksi Jasa</span></a>
                </li>

                <li>
                    <a href="<?php echo site_url('laporan/get_insentif');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Insentif Mekanik</span></a>
                </li>

                <li>
                    <a href="<?php echo site_url('laporan/get_excel');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Lbb</span></a>
                </li>

                <li>
                    <a href="<?php echo site_url('laporan/get_lbb_periodik');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Lbb Periodik</span></a>
                </li>
                
                <li>
                    <a href="<?php echo site_url('laporan/get_excel1');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Mekanik</span></a>
                </li>
                <li>
                    <a href="<?php echo site_url('laporan/get_omset_sk_periode');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Omset Sukucadang Periodik</span></a>
                </li>
                <li>
                    <a href="<?php echo site_url('laporan/get_omset_jasa_periode');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Omset Jasa Periodik</span></a>
                </li>
               
                
            </ul>
        </li>
         <li class="treeview">
            <a href="#">
            <i class="fa fa-print "></i> <span>Lap. Penjualan Sparepart</span><i class="fa fa-angle-left pull-right"></i></a>
            <ul class="treeview-menu">
                
                
                <li>
                    <a target="_blank" href="<?php echo site_url('laporan/cetak_transaksi_selling_part_all');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Tr Penjualan Part</span></a>
                </li>

                <li>
                    <a href="<?php echo site_url('laporan/get_selling_sukucadang');?>">
                    <i class="fa fa-circle-o"></i> <span>Lap. Tr Penjualan Part Periodik</span></a>
                </li>

               
                
            </ul>
        </li>

        <li class="treeview">
            <a href="<?php echo site_url('login/logout');?>">
        <i class="fa fa-arrow-left"></i> <span>Logout</span></a></li>


      <?php } ?>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">