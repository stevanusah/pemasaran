<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 *  Copyright 2013
 * 
 */

/**
 * Description of MY_Controller
 *
 * @author Daud Simbolon <daud.simbolon@gmail.com>
 */
class MY_Controller extends CI_Controller
{

    // public function __construct() 
    // {
    //     parent::__construct();
    // }
    function render_page($content, $data = NULL){
    /*
     * $data['headernya'] , $data['contentnya'] , $data['footernya']
     * variabel diatas ^ nantinya akan dikirim ke file views/template/index.php
     * */
        $data['main'] = $this->load->view($content, $data, TRUE);
        
        
        $this->load->view('blank', $data);
    }
}

?>
