<?php

class global_m extends CI_Model{    

    function get_employee($id)
	{
	   $this->db->select('id_employee');
	   $this->db->from('users');
	   $this->db->where('id', $id);
	   $res = $this->db->get();
	   if($res->num_rows()>0)
	   {
	     $row = $res->row();
	     return $row->id_employee;
	   }
	   else
	   {
	     return '';
	   }
	}

	function get_one_field($field, $table, $where, $params)
	{
	 	$this->db->select($field);
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
	   	$res = $this->db->get();
	   	if($res->num_rows()>0)
	   	{
	    	$row = $res->row();
	     	return $row->$field;
	   	}
	   	else
	   	{
	     	return '';
	   	}	
	}
	
	function get_one_field1($field, $table, $where, $params,$where1,$params1)
	{
	 	$this->db->select($field);
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
		$this->db->where($where1, $params1);
	   	$res = $this->db->get();
	   	if($res->num_rows()>0)
	   	{
	    	$row = $res->row();
	     	return $row->$field;
	   	}
	   	else
	   	{
	     	return '';
	   	}	
	}

	function get_get($query)
	{
	 	$d= $this->db->query($query);
	 	return $d->row();
	}
	
	function get_one_fields($field, $table, $where, $params,$where2, $params2,$where3, $params3)
	{
	 	$this->db->select($field);
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
		$this->db->where($where2, $params2);
		$this->db->where($where3, $params3);
	   	$res = $this->db->get();
	   	if($res->num_rows()>0)
	   	{
	    	$row = $res->row();
	     	return $row->$field;
	   	}
	   	else
	   	{
	     	return '';
	   	}	
	}
	
	
	
	function delete_one($key, $table, $param)
	{	
		$update = array(
    		'stat' => '0'
		);

		$this->db->where($key, $param);
		$this->db->update($table, $update);
	}

	function inisialisasi_delete($table, $where, $params)
	{
		$this->db->select('count(*) as total');
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
	   	$res = $this->db->get();
	   	$row = $res->row();
	   	if($row->total == 0) {
	   		return true;
	   	} else {
	   		return false;
	   	}
	}

	function insert_validation($table, $where, $params)
	{	
		$this->db->select('count(*) as total');
	   	$this->db->from($table);
	   	$this->db->where(trim($where," "), trim($params, " "));
	   	$res = $this->db->get();
	   	$row = $res->row();
	   	if($row->total != 0) {
	   		return true;
	   	} else {
	   		return false;
	   	}
	}

	function counting_data($table, $where, $params)
	{
		$this->db->select('count(*) as total');
	   	$this->db->from($table);
	   	$this->db->where($where, $params);
	   	$res = $this->db->get();
	   	return $res->row()->total;
	}
 function yahai(){
 	$query = $this->db->query("SELECT COUNT(a.id_cashbon) as total from t_cashbon a JOIN t_konfirmasi b on a.id_cashbon=b.id_cashbon WHERE a.status_cashbon=0 and b.ket_konfirmasi='-'");
 	return $query->row()->total;
 }
 function anjing(){
 	$query = $this->db->query("SELECT SUM(harga_jasa * qty_jasa - diskon)as total from detail_transaksi_jasa group by id_transaksi");
 	return $query->row();
 }
	
}